<?php

declare(strict_types=1);

namespace Thrust\Security\Foundation\Value;

use Thrust\Security\Contract\Value\SecurityValue;

class AnonymousKey extends SecurityKey
{

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->key === $aValue->getKey();
    }
}
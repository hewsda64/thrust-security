<?php

declare(strict_types=1);

namespace Thrust\Security\Foundation\Value;

use Assert\Assertion;
use Thrust\Security\Contract\Value\SecurityKey as SecurityKeyContract;

abstract class SecurityKey implements SecurityKeyContract
{
    /**
     * @var string
     */
    protected $key;

    /**
     * SecurityKey constructor.
     *
     * @param string $key
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(string $key)
    {
        Assertion::notEmpty($key, 'Security key cannot be empty');
        $this->key = $key;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function __toString(): string
    {
        return $this->getKey();
    }
}
<?php

namespace Thrust\Security\Foundation;

use Thrust\Security\Contract\Authentication\Authenticatable;
use Thrust\Security\Contract\Token\Tokenable;

trait Guard
{
    use HasToken;

    protected function authenticate(Tokenable $token): Tokenable
    {
        return app(Authenticatable::class)->authenticate($token);
    }
}
<?php

namespace Thrust\Security\Foundation;

use Thrust\Security\Contract\Token\Storage\TokenStorage;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Exception\AuthenticationCredentialsNotFound;

trait HasToken
{
    private function storage(): TokenStorage
    {
        return app(TokenStorage::class);
    }

    protected function token(): ?Tokenable
    {
        return $this->storage()->getToken();
    }

    protected function requireToken(): Tokenable
    {
        if (null === $token = $this->token()) {
            throw new AuthenticationCredentialsNotFound(
                'Token storage contains no authentication token.');
        }

        return $token;
    }

    protected function setToken(Tokenable $token = null): void
    {
        $this->storage()->setToken($token);
    }

    protected function hasToken(): bool
    {
        return null !== $this->token();
    }

    protected function eraseStorage(): void
    {
        $this->storage()->setToken();
    }
}
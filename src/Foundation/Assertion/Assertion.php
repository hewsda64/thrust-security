<?php

declare(strict_types=1);

namespace Thrust\Security\Foundation\Assertion;

use Assert\Assertion as BaseAssertion;
use Thrust\Security\Foundation\Exception\SecurityValidationException;

class Assertion extends BaseAssertion
{
    /**
     * @var string
     */
    protected static $exceptionClass = SecurityValidationException::class;
}
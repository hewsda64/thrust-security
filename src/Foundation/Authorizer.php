<?php

declare(strict_types=1);

namespace Thrust\Security\Foundation;

use Thrust\Security\Contract\Authorization\Authorizable;
use Thrust\Security\Contract\Exception\AuthorizationException;
use Thrust\Security\Contract\Token\Tokenable;

trait Authorizer
{
    protected function grant(Tokenable $token, $attributes, $object = null): bool
    {
        return app(Authorizable::class)->decide($token, (array)$attributes, $object);
    }

    protected function requireGranted(Tokenable $token, $attributes, $object = null): bool
    {
        if (!$this->grant($token, $attributes, $object)) {
            throw new AuthorizationException('Access denied.');
        }

        return true;
    }
}
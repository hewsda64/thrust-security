<?php

declare(strict_types=1);

namespace Thrust\Security\Foundation;

use Illuminate\Http\Request;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Event\UserWasImpersonated;
use Thrust\Security\Event\UserWasLogin;
use Thrust\Security\Event\UserWasLogout;

trait SecurityEventable
{
    private function dispatchEvent($event): void
    {
        app('events')->dispatch($event);
    }

    protected function dispatchLoginEvent(Request $request, Tokenable $token): void
    {
        $loginEvent = new UserWasLogin($request, $token);

        $this->dispatchEvent($loginEvent);
    }

    protected function dispatchLogoutEvent(Tokenable $token): void
    {
        $logoutEvent = new UserWasLogout($token);

        $this->dispatchEvent($logoutEvent);
    }

    protected function dispatchImpersonatedUserEvent(Request $request, Tokenable $token): void
    {
        $switchUserEvent = new UserWasImpersonated($request, $token->user());

        $this->dispatchEvent($switchUserEvent);
    }
}
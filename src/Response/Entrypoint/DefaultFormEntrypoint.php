<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Entrypoint;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Response\Entrypoint;

class DefaultFormEntrypoint implements Entrypoint
{
    /**
     * @var ResponseFactory
     */
    private $response;

    /**
     * @var string
     */
    private $target;

    /**
     * DefaultFormEntrypoint constructor.
     *
     * @param ResponseFactory $response
     * @param string $target
     */
    public function __construct(ResponseFactory $response, string $target = '/auth/login')
    {
        $this->response = $response;
        $this->target = $target;
    }

    public function start(Request $request, AuthenticationException $exception = null): Response
    {
        $message = $exception ? $exception->getMessage() : 'You must login first.';

        return $this->response->redirectTo($this->target)->with('message', $message);
    }
}
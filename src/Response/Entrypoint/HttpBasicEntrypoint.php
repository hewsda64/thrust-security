<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Entrypoint;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Response\Entrypoint;

class HttpBasicEntrypoint implements Entrypoint
{
    /**
     * @var string
     */
    protected $realmName;

    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * HttpBasicEntrypoint constructor.
     *
     * @param ResponseFactory $responseFactory
     * @param string $realmName
     */
    public function __construct(ResponseFactory $responseFactory, string $realmName = 'Private Access')
    {
        $this->realmName = $realmName;
        $this->responseFactory = $responseFactory;
    }

    public function start(Request $request, AuthenticationException $exception = null): Response
    {
        $content = view('errors.' . Response::HTTP_UNAUTHORIZED);
        $headers = ['WWW-Authenticate' => sprintf('Basic realm="%s"', $this->realmName)];

        return $this->responseFactory->make($content, Response::HTTP_UNAUTHORIZED, $headers);
    }
}
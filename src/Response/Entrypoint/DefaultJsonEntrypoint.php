<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Entrypoint;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Response\Entrypoint;

class DefaultJsonEntrypoint implements Entrypoint
{

    public function start(Request $request, AuthenticationException $exception = null): Response
    {
        $data = [
            'code' => $code = Response::HTTP_UNAUTHORIZED,
            'message' => 'You must login first.',
        ];

        return new JsonResponse($data, $code);
    }
}
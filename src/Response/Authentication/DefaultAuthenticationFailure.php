<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Authentication;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Response\AuthenticationFailure;

class DefaultAuthenticationFailure implements AuthenticationFailure
{
    /**
     * @var ResponseFactory
     */
    private $response;

    /**
     * @var string
     */
    private $target;

    /**
     * DefaultAuthenticationFailureHandler constructor.
     *
     * @param ResponseFactory $responseFactory
     * @param string $target
     */
    public function __construct(ResponseFactory $responseFactory, string $target = '/login')
    {
        $this->response = $responseFactory;
        $this->target = $target;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        return $this->response
            ->redirectTo($this->target)
            ->with('message', $exception->getMessage());
    }
}
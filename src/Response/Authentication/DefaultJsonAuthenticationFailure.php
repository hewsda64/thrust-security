<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Authentication;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Response\AuthenticationFailure;

class DefaultJsonAuthenticationFailure implements AuthenticationFailure
{

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        $message = $exception ? $exception->getMessage() : 'You must login first.';

        return new JsonResponse(['error' => $message], Response::HTTP_UNAUTHORIZED);
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Authentication;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Http\Response\AuthenticationSuccess;
use Thrust\Security\Contract\Token\Tokenable;

class DefaultSuccessAuthentication implements AuthenticationSuccess
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $target;

    /**
     * DefaultAuthenticationSuccessHandler constructor.
     *
     * @param ResponseFactory $responseFactory
     * @param string $target
     */
    public function __construct(ResponseFactory $responseFactory, string $target = '/')
    {
        $this->responseFactory = $responseFactory;
        $this->target = $target;
    }

    public function onAuthenticationSuccess(Request $request, Tokenable $token): Response
    {
        return $this->responseFactory
            ->redirectTo($this->target)
            ->with('message', 'Login successful.');
    }
}
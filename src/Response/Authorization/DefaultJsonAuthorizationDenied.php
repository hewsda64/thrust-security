<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Authorization;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthorizationException;
use Thrust\Security\Contract\Http\Response\AuthorizationDenied;

class DefaultJsonAuthorizationDenied implements AuthorizationDenied
{

    public function handle(Request $request, AuthorizationException $accessDeniedException): Response
    {
        $message = $accessDeniedException->getMessage() ?? 'Access denied.';

        return new JsonResponse(['error' => $message], Response::HTTP_UNAUTHORIZED);
    }
}
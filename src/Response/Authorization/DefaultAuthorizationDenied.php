<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Authorization;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthorizationException;
use Thrust\Security\Contract\Http\Response\AuthorizationDenied;

class DefaultAuthorizationDenied implements AuthorizationDenied
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $target;

    /**
     * DefaultDeniedHandler constructor.
     *
     * @param ResponseFactory $responseFactory
     * @param string $target
     */
    public function __construct(ResponseFactory $responseFactory, string $target = '/')
    {
        $this->responseFactory = $responseFactory;
        $this->target = $target;
    }

    public function handle(Request $request, AuthorizationException $accessDeniedException): Response
    {
        $message = $accessDeniedException->getMessage() ?? 'Access denied.';

        return $this->responseFactory->redirectTo($this->target)->with('message', $message);
    }
}
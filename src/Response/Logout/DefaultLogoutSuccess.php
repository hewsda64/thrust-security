<?php

declare(strict_types=1);

namespace Thrust\Security\Response\Logout;

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Http\Response\LogoutSuccess;

class DefaultLogoutSuccess implements LogoutSuccess
{
    /**
     * @var ResponseFactory
     */
    private $responseFactory;

    /**
     * @var string
     */
    private $target;

    /**
     * DefaultLogoutSuccessHandler constructor.
     *
     * @param ResponseFactory $responseFactory
     * @param string $target
     */
    public function __construct(ResponseFactory $responseFactory, string $target = '/')
    {
        $this->responseFactory = $responseFactory;
        $this->target = $target;
    }

    public function onLogoutSuccess(Request $request): Response
    {
        return $this->responseFactory->redirectTo($this->target);
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Storage;

use Thrust\Security\Contract\Token\Storage\TokenStorage as Storage;
use Thrust\Security\Contract\Token\Tokenable;

class TokenStorage implements Storage
{
    /**
     * @var Tokenable
     */
    private $token;

    public function getToken(): ?Tokenable
    {
        return $this->token;
    }

    public function setToken(Tokenable $token = null): void
    {
        $this->token = $token;
    }
}
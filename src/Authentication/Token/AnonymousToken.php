<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token;

use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Foundation\Value\AnonymousKey;

class AnonymousToken extends Token
{
    /**
     * @var AnonymousKey
     */
    private $anonymousKey;

    public function __construct(AnonymousKey $anonymousKey, Identifier $user, array $roles = [])
    {
        parent::__construct($roles);

        $this->setUser($user);
        $this->setAuthenticated(true);

        $this->anonymousKey = $anonymousKey;
    }

    public function credentials(): Credentials
    {
        return new EmptyCredentials();
    }

    public function getKey(): AnonymousKey
    {
        return $this->anonymousKey;
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token;

use Thrust\Security\Authentication\Token\Concerns\HasAttributes;
use Thrust\Security\Authentication\Token\Concerns\HasSerializer;
use Thrust\Security\Authentication\Token\Concerns\HasUserChanged;
use Thrust\Security\Contract\Role\Role as RoleContract;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Role\Value\Role;

abstract class Token implements Tokenable, \Serializable
{
    use HasAttributes, HasUserChanged, HasSerializer;

    /**
     * @var Identifier|User
     */
    private $user;

    /**
     * @var array
     */
    private $roles = [];

    /**
     * @var bool
     */
    private $authenticated = false;

    /**
     * Token constructor.
     *
     * @param array $roles
     * @throws \InvalidArgumentException
     */
    public function __construct(array $roles = [])
    {
        foreach ($roles as $role) {
            if (is_string($role)) {
                $role = new Role($role);
            } elseif (!$role instanceof RoleContract) {
                throw new \InvalidArgumentException(
                    sprintf('Role must be a string or implement "%s"', RoleContract::class)
                );
            }

            $this->roles [] = $role;
        }
    }

    public function roles(): array
    {
        return $this->roles;
    }

    public function setUser($user): void
    {
        $this->guardTokenUser($user);

        $this->user = $user;
    }

    public function user()
    {
        return $this->user;
    }

    public function identifier(): Identifier
    {
        if ($this->user instanceof User) {
            return $this->user->getIdentifier();
        }

        return $this->user;
    }

    public function isAuthenticated(): bool
    {
        return $this->authenticated;
    }

    public function setAuthenticated(bool $authenticated): void
    {
        $this->authenticated = $authenticated;
    }

    public function eraseCredentials(): void
    {
    }
}
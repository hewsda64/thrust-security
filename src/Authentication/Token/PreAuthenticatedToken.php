<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token;

use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Foundation\Value\ProviderKey;

class PreAuthenticatedToken extends Token
{
    /**
     * @var Credentials
     */
    private $credentials;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    public function __construct(Identifier $user, Credentials $credentials, ProviderKey $providerKey, array $roles = [])
    {
        parent::__construct($roles);

        $this->setUser($user);

        $this->credentials = $credentials;
        $this->providerKey = $providerKey;

        count($roles) and $this->setAuthenticated(true);
    }

    public function credentials(): Credentials
    {
        return $this->credentials;
    }

    public function providerKey(): ProviderKey
    {
        return $this->providerKey;
    }

    public function eraseCredentials(): void
    {
        parent::eraseCredentials();

        $this->credentials = new EmptyCredentials();
    }

    public function serialize(): string
    {
        return serialize([$this->credentials, $this->providerKey, parent::serialize()]);
    }

    public function unserialize($string): void
    {
        [$this->credentials, $this->providerKey, $parentStr] = unserialize($string, [PreAuthenticatedToken::class]);

        parent::unserialize($parentStr);
    }
}
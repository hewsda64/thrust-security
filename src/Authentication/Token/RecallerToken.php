<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token;

use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Foundation\Value\RecallerKey;

class RecallerToken extends Token
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var RecallerKey
     */
    private $recallerKey;

    /**
     * RecallerToken constructor.
     *
     * @param User $user
     * @param ProviderKey $providerKey
     * @param RecallerKey $recallerKey
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(User $user, ProviderKey $providerKey, RecallerKey $recallerKey)
    {
        parent::__construct($user->getRoles());

        $this->setUser($user);
        $this->providerKey = $providerKey;
        $this->recallerKey = $recallerKey;

        parent::setAuthenticated(true);
    }

    public function credentials(): Credentials
    {
        return new EmptyCredentials();
    }

    public function providerKey(): ProviderKey
    {
        return $this->providerKey;
    }

    public function recallerKey(): RecallerKey
    {
        return $this->recallerKey;
    }

    public function serialize(): string
    {
        return serialize([$this->recallerKey, $this->providerKey, parent::serialize()]);
    }

    public function unserialize($serialized): void
    {
        [$this->recallerKey, $this->providerKey, $parentStr] = unserialize($serialized, [RecallerToken::class]);

        parent::unserialize($parentStr);
    }
}
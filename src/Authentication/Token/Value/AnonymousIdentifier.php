<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Value;

use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Contract\Value\SecurityValue;

final class AnonymousIdentifier implements Identifier
{
    const IDENTIFIER = '.anon';

    public function identify(): string
    {
        return static::IDENTIFIER;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && static::IDENTIFIER === $aValue::IDENTIFIER;
    }

    public function __toString()
    {
        return $this->identify();
    }
}
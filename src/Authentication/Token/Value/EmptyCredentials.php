<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Value;

use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\SecurityValue;

final class EmptyCredentials implements Credentials
{

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this;
    }

    public function getCredentials(): string
    {
        return '';
    }

    public function __toString(): string
    {
        return $this->getCredentials();
    }
}
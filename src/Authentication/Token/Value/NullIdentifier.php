<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Value;

use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Contract\Value\SecurityValue;

final class NullIdentifier implements Identifier
{
    public function identify()
    {
        return null;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this;
    }
}
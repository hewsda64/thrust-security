<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Value;

use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\SecurityValue;
use Thrust\Security\Foundation\Assertion\Assertion;

class SimpleCredentials implements Credentials
{
    /**
     * @var string
     */
    private $credentials;

    /**
     * SimpleCredentials constructor.
     *
     * @param string $credentials
     */
    private function __construct(string $credentials)
    {
        $this->credentials = $credentials;
    }

    public static function fromString($credentials)
    {
        if (null === $credentials || (is_string($credentials) && empty($credentials))) {
            return new EmptyCredentials();
        }

        Assertion::string($credentials, 'Invalid credentials.');

        return new self($credentials);
    }

    public function getCredentials(): string
    {
        return $this->credentials;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->credentials === $aValue->getCredentials();
    }
}
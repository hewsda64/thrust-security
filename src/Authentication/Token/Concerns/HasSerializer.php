<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Concerns;

use Thrust\Security\Contract\Token\Tokenable;

trait HasSerializer
{
    public function serialize(): string
    {
        return serialize([
            is_object($this->user) ? clone $this->user : $this->user,
            $this->authenticated,
            array_map(function ($role) { return clone $role; }, $this->roles),
            $this->attributes
        ]);
    }

    public function unserialize($serialized): void
    {
        [$this->user, $this->authenticated, $this->roles, $this->attributes] = unserialize($serialized, [Tokenable::class]);
    }

    public function __toString(): string
    {
        return (string)$this->user;
    }
}
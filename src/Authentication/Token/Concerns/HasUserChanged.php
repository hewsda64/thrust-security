<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Concerns;

use Thrust\Security\Contract\User\LocalUser;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\User\UserThrottle;
use Thrust\Security\Contract\Value\Entity;
use Thrust\Security\Contract\Value\Identifier;

trait HasUserChanged
{
    protected function guardTokenUser($user): void
    {
        if (!$user instanceof User && !$user instanceof Identifier) {
            throw new \InvalidArgumentException(
                sprintf('User must implement an Identifier contract "%s" or a User contract "%s"',
                    Identifier::class, User::class)
            );
        }

        $changed = true;

        if (null === $this->user) {
            $changed = false;
        } elseif ($this->user instanceof User) {
            $changed = !$user instanceof User ? true : $this->hasUserChanged($user);
        } elseif ($user instanceof User) {
            $changed = true;
        }

        $changed and $this->setAuthenticated(false);
    }

    private function hasUserChanged(User $user): bool
    {
        if (!$this->user instanceof User) {
            throw new \BadMethodCallException(
                sprintf('Method %s must be called when current user implement User contract %s',
                    __METHOD__, User::class)
            );
        }

        if ($this->user instanceof Entity && $user instanceof Entity) {
            return !$this->user->sameIdentityAs($user);
        }

        if ($this->user instanceof Entity xor $user instanceof Entity) {
            return true;
        }

        if ($this->user instanceof LocalUser
            && $user instanceof LocalUser
            && !$this->user->getPassword()->sameValueAs($user->getPassword())) {
            return true;
        }

        if ($this->user instanceof LocalUser xor $user instanceof LocalUser) {
            return true;
        }

        if (!$this->user->getUserName()->sameValueAs($user->getUserName())) {
            return true;
        }

        if ($this->user instanceof UserThrottle && $user instanceof UserThrottle) {
            if ($this->user->isEnabled() !== $user->isEnabled()) {
                return true;
            }
        }

        return $this->user instanceof UserThrottle xor $user instanceof UserThrottle;
    }
}
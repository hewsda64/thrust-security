<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token\Concerns;

trait HasAttributes
{
    /**
     * @var array
     */
    private $attributes = [];

    public function __get(string $key)
    {
        return $this->attributes[$key];
    }

    public function __set(string $key, $value): void
    {
        $this->attributes[$key] = $value;
    }

    public function __isset(string $key): bool
    {
        return isset($this->attributes[$key]);
    }

    public function __unset(string $key)
    {
        unset($this->attributes[$key]);
    }
}
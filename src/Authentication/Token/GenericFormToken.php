<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Token;

use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Foundation\Value\ProviderKey;

class GenericFormToken extends Token
{

    /**
     * @var Credentials
     */
    private $credentials;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * GenericFormToken constructor.
     * @param Identifier|User $user
     * @param Credentials $credentials
     * @param ProviderKey $providerKey
     * @param array $roles
     *
     * @throws \InvalidArgumentException
     */
    public function __construct($user, Credentials $credentials, ProviderKey $providerKey, array $roles = [])
    {
        parent::__construct($roles);

        $this->setUser($user);
        $this->setAuthenticated(count($roles) > 0);

        $this->credentials = $credentials;
        $this->providerKey = $providerKey;
    }

    public function eraseCredentials(): void
    {
        $this->credentials = new EmptyCredentials();
    }

    public function credentials(): Credentials
    {
        return $this->credentials;
    }

    public function providerKey(): ProviderKey
    {
        return $this->providerKey;
    }

    public function serialize(): string
    {
        return serialize([$this->credentials, $this->providerKey, parent::serialize()]);
    }

    public function unserialize($serialized): void
    {
        [$this->credentials, $this->providerKey, $parentStr] = unserialize($serialized, [Tokenable::class]);

        parent::unserialize($parentStr);
    }
}
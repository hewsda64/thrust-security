<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication;

use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Token\Tokenable;

class AuthenticationProviders
{
    /**
     * @var array
     */
    private $authenticationProviders;

    /**
     * AuthenticationProviders constructor.
     *
     * @param array $authenticationProviders
     */
    public function __construct(array $authenticationProviders = [])
    {
        $this->authenticationProviders = $authenticationProviders;
    }

    public function addProvider(AuthenticationProvider $provider): AuthenticationProviders
    {
        $this->authenticationProviders[] = $provider;

        return $this;
    }

    public function getFirstProviderSupported(Tokenable $token): ?AuthenticationProvider
    {
        return array_first($this->authenticationProviders, function (AuthenticationProvider $provider) use ($token) {
            return $provider->supports($token);
        });
    }
}
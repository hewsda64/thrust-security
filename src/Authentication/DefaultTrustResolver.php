<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication;

use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Token\Tokenable;

class DefaultTrustResolver implements TrustResolver
{

    /**
     * @var string
     */
    private $anonymousClass;

    /**
     * @var string
     */
    private $rememberMeClass;

    /**
     * DefaultTrustResolver constructor.
     *
     * @param string $anonymousClass
     * @param string $rememberMeClass
     */
    public function __construct(string $anonymousClass, string $rememberMeClass)
    {
        $this->anonymousClass = $anonymousClass;
        $this->rememberMeClass = $rememberMeClass;
    }

    public function isFullFledged(Tokenable $token = null): bool
    {
        if (!$token) {
            return false;
        }

        return !$this->isAnonymous($token) && !$this->isRememberMe($token);
    }

    public function isAnonymous(Tokenable $token = null): bool
    {
        if (!$token) {
            return false;
        }

        return $token instanceof $this->anonymousClass;
    }

    public function isRememberMe(Tokenable $token = null): bool
    {
        if (!$token) {
            return false;
        }

        return $token instanceof $this->rememberMeClass;
    }
}
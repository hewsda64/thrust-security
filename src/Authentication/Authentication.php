<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication;

use Thrust\Security\Contract\Authentication\Authenticatable;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Exception\UnsupportedAuthenticationProvider;

class Authentication implements Authenticatable
{
    /**
     * @var AuthenticationProviders
     */
    private $providers;

    /**
     * Authentication constructor.
     *
     * @param AuthenticationProviders $providers
     */
    public function __construct(AuthenticationProviders $providers)
    {
        $this->providers = $providers;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        $provider = $this->providers->getFirstProviderSupported($token);

        if (!$provider) {
            throw new UnsupportedAuthenticationProvider(
                sprintf('No authentication provider supports token %s', get_class($token))
            );
        }

        return $provider->authenticate($token);
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Provider;

use Thrust\Security\Authentication\Token\RecallerToken;
use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\UserChecker;
use Thrust\Security\Exception\BadCredentials;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Foundation\Value\RecallerKey;

class RecallerAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var ProviderKey
     */
    protected $providerKey;

    /**
     * @var UserChecker
     */
    protected $userChecker;

    /**
     * @var string
     */
    protected $recallerKey;

    /**
     * RecallerAuthenticationProvider constructor.
     *
     * @param ProviderKey $providerKey
     * @param RecallerKey $recallerKey
     * @param UserChecker $userChecker
     */
    public function __construct(ProviderKey $providerKey, RecallerKey $recallerKey, UserChecker $userChecker)
    {
        $this->providerKey = $providerKey;
        $this->recallerKey = $recallerKey;
        $this->userChecker = $userChecker;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        if (!$this->recallerKey->sameValueAs($token->recallerKey())) {
            throw new BadCredentials('The presented key does not match.');
        }

        $user = $token->user();

        $this->userChecker->checkPreAuthentication($user);

        return new RecallerToken($user, $this->providerKey, $this->recallerKey);
    }

    public function supports(Tokenable $token): bool
    {
        return get_class($token) === RecallerToken::class
            && $token->providerKey()->sameValueAs($this->providerKey);
    }
}
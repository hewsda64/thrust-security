<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Provider;

use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Authentication\Authenticator\SimpleAuthenticator;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\UserProvider;
use Thrust\Security\Foundation\Value\ProviderKey;

class SimplePreAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var SimpleAuthenticator
     */
    private $authenticator;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * SimplePreAuthenticationProvider constructor.
     *
     * @param SimpleAuthenticator $authenticator
     * @param UserProvider $userProvider
     * @param ProviderKey $providerKey
     */
    public function __construct(SimpleAuthenticator $authenticator, UserProvider $userProvider, ProviderKey $providerKey)
    {
        $this->authenticator = $authenticator;
        $this->userProvider = $userProvider;
        $this->providerKey = $providerKey;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        return $this->authenticator->authenticateToken($token, $this->userProvider, $this->providerKey);
    }

    public function supports(Tokenable $token): bool
    {
        return $this->authenticator->supportsToken($token, $this->providerKey);
    }
}
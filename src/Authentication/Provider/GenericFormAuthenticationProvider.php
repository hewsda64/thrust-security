<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Provider;

use Illuminate\Contracts\Hashing\Hasher;
use Thrust\Security\Authentication\Token\GenericFormToken;
use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\LocalUser;
use Thrust\Security\Contract\User\User as UserContract;
use Thrust\Security\Contract\User\UserChecker;
use Thrust\Security\Contract\User\UserProvider;
use Thrust\Security\Exception\BadCredentials;
use Thrust\Security\Exception\UnsupportedAuthenticationProvider;
use Thrust\Security\Exception\UserNotFound;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Role\Value\SwitchUserRole;

class GenericFormAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var UserProvider
     */
    private $userProvider;

    /**
     * @var UserChecker
     */
    private $userChecker;

    /**
     * @var bool
     */
    private $hideException;

    /**
     * @var Hasher
     */
    private $encoder;

    /**
     * GenericFormAuthenticationProvider constructor.
     *
     * @param ProviderKey $providerKey
     * @param UserProvider $userProvider
     * @param UserChecker $userChecker
     * @param Hasher $encoder
     * @param bool $hideException
     */
    public function __construct(ProviderKey $providerKey,
                                UserProvider $userProvider,
                                UserChecker $userChecker,
                                Hasher $encoder,
                                bool $hideException = true)
    {
        $this->providerKey = $providerKey;
        $this->userProvider = $userProvider;
        $this->userChecker = $userChecker;
        $this->hideException = $hideException;
        $this->encoder = $encoder;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        if (!$this->supports($token)) {
            throw new UnsupportedAuthenticationProvider(
                sprintf('Authentication provider "%s" does not support token "%s"',
                    get_class($this), get_class($token))
            );
        }

        try {
            $user = $this->retrieveUser($token);

            $this->checkUser($user, $token);
        } catch (BadCredentials $badCredentials) {
            if ($this->hideException) {
                throw new UserNotFound('User not found.', 0, $badCredentials);
            }

            throw $badCredentials;
        }

        return $this->createAuthenticatedToken($user, $token);
    }

    private function retrieveUser(Tokenable $token): LocalUser
    {
        if ($token->user() instanceof UserContract) {
            return $token->user();
        }

        return $this->userProvider->requireByIdentifier($token->identifier());
    }

    private function checkUser(LocalUser $user, Tokenable $token): void
    {
        $this->userChecker->checkPreAuthentication($user);

        $this->checkCredentials($user, $token);

        $this->userChecker->checkPostAuthentication($user);
    }

    private function createAuthenticatedToken(LocalUser $user, Tokenable $token): GenericFormToken
    {
        return new GenericFormToken(
            $user,
            $token->credentials(),
            $this->providerKey,
            $this->getRoles($user, $token)
        );
    }

    private function getRoles(LocalUser $user, Tokenable $token): array
    {
        $roles = $user->getRoles();

        foreach ($token->roles() as $role) {
            if ($role instanceof SwitchUserRole) {
                $roles[] = $role;
                break;
            }
        }

        return $roles;
    }

    private function checkCredentials(LocalUser $user, Tokenable $token): void
    {
        $currentUser = $token->user();

        if ($currentUser instanceof LocalUser) {
            if (!$currentUser->getPassword()->sameValueAs($user->getPassword())) {
                throw new BadCredentials('The credentials were changed from another session.');
            }
        } else {
            $presentedPassword = $token->credentials();

            if ($presentedPassword instanceof EmptyCredentials) {
                throw new BadCredentials('The presented password cannot be empty.');
            }

            if (!$this->encoder->check($presentedPassword->getCredentials(), $user->getPassword()->getCredentials())) {
                throw new BadCredentials('The presented password is invalid.');
            }
        }
    }

    public function supports(Tokenable $token): bool
    {
        return $token instanceof GenericFormToken &&
            $this->providerKey->sameValueAs($token->providerKey());
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Authentication\Provider;

use Thrust\Security\Authentication\Token\AnonymousToken;
use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Exception\UnsupportedAuthenticationProvider;
use Thrust\Security\Foundation\Value\AnonymousKey;

class AnonymousAuthenticationProvider implements AuthenticationProvider
{
    /**
     * @var AnonymousKey
     */
    private $key;

    /**
     * AnonymousAuthenticationProvider constructor.
     *
     * @param AnonymousKey $key
     */
    public function __construct(AnonymousKey $key)
    {
        $this->key = $key;
    }

    public function authenticate(Tokenable $token): Tokenable
    {
        if (!$this->supports($token)) {
            throw new UnsupportedAuthenticationProvider(
                sprintf('Authentication provider "%s" does not support token "%s"',
                    get_class($this), get_class($token))
            );
        }

        return $token;
    }

    public function supports(Tokenable $token): bool
    {
        return $token instanceof AnonymousToken && $this->key->sameValueAs($token->getKey());
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Role\Value;

use Thrust\Security\Contract\Role\Role as RoleContract;
use Thrust\Security\Contract\Value\SecurityValue;

class Role implements RoleContract, SecurityValue
{
    /**
     * @var string
     */
    private $role;

    /**
     * Role constructor.
     *
     * @param string $role
     */
    public function __construct(string $role)
    {
        $this->role = $role;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->role === $aValue->getName();
    }

    public function getName(): string
    {
        return $this->role;
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}
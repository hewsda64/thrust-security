<?php

declare(strict_types=1);

namespace Thrust\Security\Role\Value;

use Thrust\Security\Contract\Token\Tokenable;

class SwitchUserRole extends Role
{
    /**
     * @var Tokenable
     */
    private $source;

    /**
     * SwitchUserRole constructor.
     *
     * @param string $role
     * @param Tokenable $source
     */
    public function __construct(string $role, Tokenable $source)
    {
        parent::__construct($role);

        $this->source = $source;
    }

    public function source(): Tokenable
    {
        return $this->source;
    }
}
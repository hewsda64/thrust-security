<?php

declare(strict_types=1);

namespace Thrust\Security\Exception;

use Thrust\Security\Contract\Exception\AuthenticationException;

class AuthenticationCredentialsNotFound extends AuthenticationException
{
}
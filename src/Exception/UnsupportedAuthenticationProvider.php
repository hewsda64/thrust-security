<?php

declare(strict_types=1);

namespace Thrust\Security\Exception;

class UnsupportedAuthenticationProvider extends AuthenticationServiceException
{
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Service\Recaller;

class Recaller
{
    const DELIMITER = '|';

    /**
     * @var string
     */
    private $recaller;

    /**
     * Recaller constructor.
     *
     * @param string $recaller
     */
    public function __construct(string $recaller)
    {
        $this->recaller = $recaller;
    }

    public function id(): string
    {
        return explode(self::DELIMITER, $this->recaller, 2)[0];
    }

    public function token(): string
    {
        return explode(self::DELIMITER, $this->recaller, 2)[1];
    }

    public function delimiter(): string
    {
        return self::DELIMITER;
    }

    public function valid(): bool
    {
        if (!str_contains($this->recaller, self::DELIMITER)) {
            return false;
        }

        $segments = explode(self::DELIMITER, $this->recaller);

        return 2 === count($segments) && trim($segments[0]) !== '' && trim($segments[1]) !== '';
    }
}
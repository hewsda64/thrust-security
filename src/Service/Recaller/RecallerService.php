<?php

declare(strict_types=1);

namespace Thrust\Security\Service\Recaller;

use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Authentication\Recaller\RecallerProvider;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Logout\Logout;
use Thrust\Security\Contract\Recaller\Recallable;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\User\UserRecaller;
use Thrust\Security\Exception\CookieTheft;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Foundation\Value\RecallerKey;

abstract class RecallerService implements Recallable, Logout
{
    /**
     * @var RecallerKey
     */
    protected $recallerKey;

    /**
     * @var ProviderKey
     */
    protected $providerKey;

    /**
     * @var CookieJar
     */
    protected $cookie;

    /**
     * @var RecallerProvider
     */
    protected $recallerProvider;

    /**
     * RecallerService constructor.
     *
     * @param RecallerKey $recallerKey
     * @param ProviderKey $providerKey
     * @param CookieJar $cookie
     * @param RecallerProvider $recallerProvider
     */
    public function __construct(RecallerKey $recallerKey,
                                ProviderKey $providerKey,
                                CookieJar $cookie,
                                RecallerProvider $recallerProvider
    )
    {
        $this->recallerKey = $recallerKey;
        $this->providerKey = $providerKey;
        $this->cookie = $cookie;
        $this->recallerProvider = $recallerProvider;
    }

    public function autoLogin(Request $request): ?Tokenable
    {
        if (null === $recaller = $this->getRecaller($request)) {
            return null;
        }

        try {
            return $this->processAutoLogin($recaller);
        } catch (CookieTheft $exception) {
            $this->cancelCookie($request);

            throw $exception;
        } catch (AuthenticationException $exception) {
        }

        $this->cancelCookie($request);

        return null;
    }

    public function loginFail(Request $request): void
    {
        $this->cancelCookie($request);

        $this->onLoginFail($request);
    }

    public function loginSuccess(Request $request, Response $response, Tokenable $token): void
    {
        $this->cancelCookie($request);

        if (!$token->user() instanceof User || !$this->isRememberMeRequested($request)) {
            return;
        }

        $this->onLoginSuccess($request, $response, $token);
    }

    public function logout(Request $request, Response $response, Tokenable $token): void
    {
        $this->cancelCookie($request);
    }

    abstract public function processAutoLogin(Recaller $recaller): Tokenable;

    abstract public function onLoginSuccess(Request $request, Response $response, Tokenable $token): void;

    abstract public function onLoginFail(Request $request);

    protected function getRecaller(Request $request): ?Recaller
    {
        if (null === $recaller = $request->cookie($this->getName())) {
            return null;
        }

        $recaller = new Recaller($recaller);

        if ($recaller->valid()) {
            return $recaller;
        }

        throw new AuthenticationException('Recaller validation failed.');
    }

    protected function createCookie(string $value): Cookie
    {
        return $this->cookie->forever($this->getName(), $value);
    }

    protected function cancelCookie(Request $request): void
    {
        if (null !== $this->getRecaller($request)) {
            $this->cookie->queue(
                $this->cookie->forget($this->getName()));
        }
    }

    protected function queue(User $user): void
    {
        if (!$user instanceof UserRecaller) {
            throw new \RuntimeException(
                sprintf('User must implement %s to use recaller service', UserRecaller::class));
        }

        $value = (string)$user->getId() . Recaller::DELIMITER . $user->getRecallerToken();

        $this->cookie->queue($this->createCookie($value));
    }

    protected function isRememberMeRequested(Request $request): bool
    {
        return
            $request->isMethod('post') &&
            in_array($request->input('remember-me'), [
                'true', 'yes', '1', 'on', 'remember-me'
            ], true);
    }

    public function getName(): string
    {
        return 'remember_' . $this->recallerKey . '_' . sha1(static::class);
    }
}
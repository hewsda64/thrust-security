<?php

declare(strict_types=1);

namespace Thrust\Security\Service\Recaller;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Authentication\Token\RecallerToken;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\User\UserRecaller;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\User\Value\UserId;

class SimpleRecallerService extends RecallerService
{

    public function processAutoLogin(Recaller $recaller): Tokenable
    {
        $user = $this->refreshRememberToken(
            $this->requireUserFromRecaller($recaller->id(), $recaller->token())
        );

        $this->queue($user);

        return new RecallerToken($user, $this->providerKey, $this->recallerKey);
    }

    protected function requireUserFromRecaller(string $identifier, string $token): User
    {
        return $this->recallerProvider->requireByRecallerIdentifier($this->makeIdentifier($identifier), $token);
    }

    protected function refreshRememberToken(User $user): User
    {
        if (!$user instanceof UserRecaller) {
            throw new \RuntimeException(
                sprintf('Class "%s" must implement a recaller contract.', get_class($user)));
        }

        return $this->recallerProvider->refreshRecaller($user, $this->createRecallerTokenString());
    }

    public function onLoginSuccess(Request $request, Response $response, Tokenable $token): void
    {
        $user = $this->refreshRememberToken($token->user());

        $this->queue($user);
    }

    public function onLoginFail(Request $request): void
    {
    }

    protected function createRecallerTokenString(): string
    {
        return str_random(60);
    }

    protected function makeIdentifier(string $identifier): Identifier
    {
        return UserId::fromString($identifier);
    }

    public function isRememberMeRequested(Request $request): bool
    {
        return parent::isRememberMeRequested($request);
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Service\Logout;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Http\Logout\Logout;
use Thrust\Security\Contract\Token\Tokenable;

class SessionLogout implements Logout
{

    public function logout(Request $request, Response $response, Tokenable $token): void
    {
        $request->session()->flush();
    }
}
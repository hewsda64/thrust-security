<?php

declare(strict_types=1);

namespace Thrust\Security\Authorization\Strategy;

use Thrust\Security\Contract\Authorization\Authorizable;
use Thrust\Security\Contract\Authorization\Votable;
use Thrust\Security\Contract\Token\Tokenable;

class UnanimousAuthorizationStrategy implements Authorizable
{
    /**
     * @var array
     */
    private $voters;

    /**
     * @var bool
     */
    private $allowIfAllAbstain;

    /**
     * UnanimousAuthorizationStrategy constructor.
     *
     * @param array $voters
     * @param bool $allowIfAllAbstain
     */
    public function __construct(array $voters, bool $allowIfAllAbstain = false)
    {
        $this->voters = $voters;
        $this->allowIfAllAbstain = $allowIfAllAbstain;
    }

    public function decide(Tokenable $token, array $attributes, $object): bool
    {
        $grant = 0;

        foreach ($attributes as $attribute) {
            foreach ($this->voters as $voter) {
                $decision = $voter->vote($token, $object, [$attribute]);

                switch ($decision) {
                    case Votable::ACCESS_GRANTED:
                        ++$grant;
                        break;
                    case Votable::ACCESS_DENIED:
                        return false;
                    default:
                        break;
                }
            }
        }

        if ($grant > 0) {
            return true;
        }

        return $this->allowIfAllAbstain;
    }
}
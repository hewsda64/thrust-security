<?php

declare(strict_types=1);

namespace Thrust\Security\Authorization\Voter;

use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Token\Tokenable;

class AnonymousVoter extends Voter
{
    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * AnonymousVoter constructor.
     *
     * @param TrustResolver $trustResolver
     */
    public function __construct(TrustResolver $trustResolver)
    {
        $this->trustResolver = $trustResolver;
    }

    protected function voteOnAttribute(string $attribute, $subject, Tokenable $token): bool
    {
        return $this->trustResolver->isAnonymous($token);
    }

    protected function supports(string $attribute, $subject): bool
    {
        return 'anonymous' === $attribute;
    }
}
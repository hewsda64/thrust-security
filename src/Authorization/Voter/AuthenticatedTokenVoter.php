<?php

declare(strict_types=1);

namespace Thrust\Security\Authorization\Voter;

use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Authorization\Votable;
use Thrust\Security\Contract\Token\Tokenable;

class AuthenticatedTokenVoter implements Votable
{
    const IS_AUTHENTICATED_FULLY = 'IS_AUTHENTICATED_FULLY';
    const IS_AUTHENTICATED_REMEMBERED = 'IS_AUTHENTICATED_REMEMBERED';
    const IS_AUTHENTICATED_ANONYMOUSLY = 'IS_AUTHENTICATED_ANONYMOUSLY';

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * AuthenticatedTokenVoter constructor.
     *
     * @param TrustResolver $trustResolver
     */
    public function __construct(TrustResolver $trustResolver)
    {

        $this->trustResolver = $trustResolver;
    }

    public function vote(Tokenable $token, $subject, array $attributes): int
    {
        $result = Votable::ACCESS_ABSTAIN;

        foreach ($attributes as $attribute) {
            if (null === $attribute || (self::IS_AUTHENTICATED_FULLY !== $attribute
                    && self::IS_AUTHENTICATED_REMEMBERED !== $attribute
                    && self::IS_AUTHENTICATED_ANONYMOUSLY !== $attribute)
            ) {
                continue;
            }

            $result = Votable::ACCESS_DENIED;

            if (self::IS_AUTHENTICATED_FULLY === $attribute
                && $this->trustResolver->isFullFledged($token)
            ) {
                return Votable::ACCESS_GRANTED;
            }

            if (self::IS_AUTHENTICATED_REMEMBERED === $attribute
                && ($this->trustResolver->isRememberMe($token)
                    || $this->trustResolver->isFullFledged($token))
            ) {
                return Votable::ACCESS_GRANTED;
            }

            if (self::IS_AUTHENTICATED_ANONYMOUSLY === $attribute
                && ($this->trustResolver->isAnonymous($token)
                    || $this->trustResolver->isRememberMe($token)
                    || $this->trustResolver->isFullFledged($token))
            ) {
                return Votable::ACCESS_GRANTED;
            }
        }

        return $result;
    }
}
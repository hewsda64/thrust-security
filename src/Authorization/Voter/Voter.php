<?php

declare(strict_types=1);

namespace Thrust\Security\Authorization\Voter;

use Thrust\Security\Contract\Authorization\Votable;
use Thrust\Security\Contract\Token\Tokenable;

abstract class Voter implements Votable
{
    public function vote(Tokenable $token, $subject, array $attributes): int
    {
        $vote = self::ACCESS_ABSTAIN;

        foreach ($attributes as $attribute) {
            if (!$this->supports($attribute, $subject)) {
                continue;
            }

            $vote = self::ACCESS_DENIED;

            if ($this->voteOnAttribute($attribute, $subject, $token)) {
                return self::ACCESS_GRANTED;
            }
        }

        return $vote;
    }

    abstract protected function supports(string $attribute, $subject): bool;

    abstract protected function voteOnAttribute(string $attribute, $subject, Tokenable $token): bool;
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Authorization\Voter;

use Assert\Assertion;
use Thrust\Security\Contract\Authorization\Votable;
use Thrust\Security\Contract\Role\Role;
use Thrust\Security\Contract\Token\Tokenable;

class RoleVoter implements Votable
{

    /**
     * @var string
     */
    private $prefix;

    /**
     * RoleVoter constructor.
     *
     * @param string $prefix
     *
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(string $prefix)
    {
        Assertion::notEmpty($prefix, 'Role prefix can not be empty.');

        $this->prefix = $prefix;
    }

    public function vote(Tokenable $token, $subject, array $attributes): int
    {
        $result = Votable::ACCESS_ABSTAIN;

        $roles = $this->extractRoles($token);

        foreach ($attributes as $attribute) {
            if (0 !== strpos($attribute, $this->prefix)) {
                continue;
            }

            $result = Votable::ACCESS_DENIED;

            /** @var Role $role */
            foreach ($roles as $role) {
                if ($attribute === $role->getName()) {
                    return Votable::ACCESS_GRANTED;
                }
            }
        }

        return $result;
    }

    protected function extractRoles(Tokenable $token): array
    {
        return $token->roles();
    }
}
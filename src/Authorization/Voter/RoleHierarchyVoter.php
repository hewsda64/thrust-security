<?php

declare(strict_types=1);

namespace Thrust\Security\Authorization\Voter;

use Thrust\Security\Contract\Role\RoleHierarchy;
use Thrust\Security\Contract\Token\Tokenable;

class RoleHierarchyVoter extends RoleVoter
{
    /**
     * @var RoleHierarchy
     */
    private $roleHierarchy;

    /**
     * RoleHierarchyVoter constructor.
     *
     * @param RoleHierarchy $roleHierarchy
     * @param string $prefix
     *
     * @throws \Assert\AssertionFailedException
     */
    public function __construct(RoleHierarchy $roleHierarchy, string $prefix)
    {
        parent::__construct($prefix);

        $this->roleHierarchy = $roleHierarchy;
    }

    protected function extractRoles(Tokenable $token): array
    {
        return $this->roleHierarchy->getReachableRoles($token->roles());
    }
}
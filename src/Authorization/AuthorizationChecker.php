<?php

declare(strict_types=1);

namespace Thrust\Security\Authorization;

use Thrust\Security\Contract\Authorization\Grantable;
use Thrust\Security\Foundation\Authorizer;
use Thrust\Security\Foundation\Guard;

class AuthorizationChecker implements Grantable
{

    use Guard, Authorizer;

    /**
     * @var bool
     */
    private $alwaysAuthenticate = false;

    public function isGranted(array $attributes = null, $object = null): bool
    {
        $token = $this->requireToken();

        if (true === $this->alwaysAuthenticate || !$token->isAuthenticated()) {
            $this->setToken($token = $this->authenticate($token));
        }

        return $this->grant($token, (array)$attributes, $object);
    }

    public function forceAuthentication(bool $force): Grantable
    {
        $this->alwaysAuthenticate = $force;

        return $this;
    }
}
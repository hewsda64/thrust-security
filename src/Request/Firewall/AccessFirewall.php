<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Thrust\Security\Contract\Http\Firewall\Firewall;
use Thrust\Security\Foundation\Authorizer;
use Thrust\Security\Foundation\Guard;

class AccessFirewall implements Firewall
{
    use Guard, Authorizer;

    /**
     * @var array
     */
    private $attributes;

    /**
     * AccessListener constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    public function handle(Request $request, \Closure $next)
    {
        $token = $this->requireToken();

        if (!$this->attributes) {
            return $next($request);
        }

        if (!$token->isAuthenticated()) {
            $token = $this->authenticate($token);

            $this->setToken($token);
        }

        $this->requireGranted($token, $this->attributes, $request);

        return $next($request);
    }

    public function setAttributes(array $attributes): void
    {
        $this->attributes = array_merge($this->attributes, $attributes);
    }
}
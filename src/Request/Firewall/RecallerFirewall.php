<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Recaller\Recallable;

class RecallerFirewall extends AuthenticationFirewall
{

    /**
     * @var Recallable
     */
    private $recallerService;

    /**
     * RecallerListener constructor.
     *
     * @param Recallable $recallerService
     */
    public function __construct(Recallable $recallerService)
    {
        $this->recallerService = $recallerService;
    }

    protected function processAuthentication(Request $request): ?Response
    {
        if (null === $token = $this->recallerService->autoLogin($request)) {
            return null;
        }

        try {
            $this->setToken($token = $this->authenticate($token));

            $this->dispatchLoginEvent($request, $token);

            return null;
        } catch (AuthenticationException $exception) {
            logger('Authentication failed with recaller service',
                ['exception' => $exception->getMessage()]);

            $this->recallerService->loginFail($request);

            throw $exception;
        }
    }

    protected function isRequired(Request $request): bool
    {
        return !$this->hasToken();
    }
}
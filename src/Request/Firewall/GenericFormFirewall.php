<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Authentication\Token\GenericFormToken;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Request\AuthenticationRequest;
use Thrust\Security\Contract\Http\Response\AuthenticationFailure;
use Thrust\Security\Contract\Http\Response\AuthenticationSuccess;
use Thrust\Security\Contract\Recaller\Recallable;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Exception\BadCredentials;
use Thrust\Security\Foundation\Value\ProviderKey;

class GenericFormFirewall extends AuthenticationFirewall
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var AuthenticationSuccess
     */
    private $successHandler;

    /**
     * @var AuthenticationFailure
     */
    private $failureHandler;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * @var bool
     */
    private $stateLess;

    /**
     * @var Recallable
     */
    protected $recallerService;

    /**
     * GenericFormListener constructor.
     *
     * @param ProviderKey $providerKey
     * @param AuthenticationSuccess $successHandler
     * @param AuthenticationFailure $failureHandler
     * @param AuthenticationRequest $authenticationRequest
     * @param bool $stateLess
     */
    public function __construct(ProviderKey $providerKey,
                                AuthenticationSuccess $successHandler,
                                AuthenticationFailure $failureHandler,
                                AuthenticationRequest $authenticationRequest,
                                bool $stateLess)
    {
        $this->providerKey = $providerKey;
        $this->successHandler = $successHandler;
        $this->failureHandler = $failureHandler;
        $this->stateLess = $stateLess;
        $this->authenticationRequest = $authenticationRequest;
    }

    protected function isRequired(Request $request): bool
    {
        return $this->authenticationRequest->matches($request);
    }

    protected function processAuthentication(Request $request): ?Response
    {
        [$identifier, $password] = $this->extractCredentialsFromRequest($request);

        try {
            $token = $this->authenticate($this->createToken($identifier, $password));

            return $this->onSuccess($request, $token);
        } catch (AuthenticationException $exception) {
            return $this->onFailure($request, $exception);
        }
    }

    protected function onSuccess(Request $request, Tokenable $token): Response
    {
        $response = $this->successHandler->onAuthenticationSuccess($request, $token);

        if ($this->recallerService) {
            $this->recallerService->loginSuccess($request, $response, $token);
        }

        if (!$this->stateLess) {
            $this->dispatchLoginEvent($request, $token);
        }

        $this->setToken($token);

        return $response;
    }

    protected function onFailure(Request $request, AuthenticationException $exception): Response
    {
        $token = $this->token();
        if ($token instanceof GenericFormToken && $this->providerKey->sameValueAs($token->providerKey())) {
            $this->eraseStorage();
        }

        return $this->failureHandler->onAuthenticationFailure($request, $exception);
    }

    protected function extractCredentialsFromRequest(Request $request): array
    {
        $credentials = $this->authenticationRequest->extract($request);

        if (!$credentials || !$credentials[0] || !$credentials[1]) {
            throw new BadCredentials('No credentials found in request.');
        }

        return $credentials;
    }

    protected function createToken(Identifier $identifier, Credentials $password): GenericFormToken
    {
        return new GenericFormToken($identifier, $password, $this->providerKey);
    }

    public function setRecaller(Recallable $recallerService): void
    {
        $this->recallerService = $recallerService;
    }
}
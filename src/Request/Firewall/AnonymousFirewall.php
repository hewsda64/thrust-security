<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Authentication\Token\AnonymousToken;
use Thrust\Security\Authentication\Token\Value\AnonymousIdentifier;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Foundation\Value\AnonymousKey;

class AnonymousFirewall extends AuthenticationFirewall
{
    /**
     * @var AnonymousKey
     */
    private $anonymousKey;

    /**
     * AnonymousListener constructor.
     *
     * @param AnonymousKey $anonymousKey
     */
    public function __construct(AnonymousKey $anonymousKey)
    {
        $this->anonymousKey = $anonymousKey;
    }

    protected function isRequired(Request $request): bool
    {
        return !$this->hasToken();
    }

    protected function processAuthentication(Request $request): ?Response
    {
        $token = new AnonymousToken($this->anonymousKey, new AnonymousIdentifier());

        try {
            $this->setToken($this->authenticate($token));
        } catch (AuthenticationException $exception) {
        }

        return null;
    }
}
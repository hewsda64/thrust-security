<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Authentication\Token\GenericFormToken;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Request\AuthenticationRequest;
use Thrust\Security\Contract\Http\Response\Entrypoint;
use Thrust\Security\Contract\User\LocalUser;
use Thrust\Security\Exception\BadCredentials;
use Thrust\Security\Foundation\Value\ProviderKey;

class HttpBasicFirewall extends AuthenticationFirewall
{
    /**
     * @var ProviderKey
     */
    private $providerKey;

    /**
     * @var Entrypoint
     */
    private $entrypoint;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * HttpBasicListener constructor.
     *
     * @param ProviderKey $providerKey
     * @param Entrypoint $entrypoint
     * @param AuthenticationRequest $authenticationRequest
     */
    public function __construct(ProviderKey $providerKey, Entrypoint $entrypoint, AuthenticationRequest $authenticationRequest)
    {
        $this->providerKey = $providerKey;
        $this->entrypoint = $entrypoint;
        $this->authenticationRequest = $authenticationRequest;
    }

    protected function isRequired(Request $request): bool
    {
        [$email] = $this->authenticationRequest->extract($request);

        if (!$email) {
            return false;
        }

        if (null !== $token = $this->token()) {
            if ($token instanceof GenericFormToken &&
                $token->isAuthenticated() &&
                $token->user() instanceof LocalUser &&
                (string)$token->user()->getEmail() === $email
            ) {
                return false;
            }
        }

        return true;
    }

    protected function processAuthentication(Request $request): ?Response
    {
        try {
            $token = $this->authenticate($this->createToken($request));

            $this->dispatchLoginEvent($request, $token);

            $this->setToken($token);

            return null;
        } catch (AuthenticationException $exception) {

            $token = $this->token();

            if ($token instanceof GenericFormToken && $this->providerKey->sameValueAs($token->providerKey())) {
                $this->eraseStorage();
            }

            return $this->entrypoint->start($request, $exception);
        }
    }

    private function createToken(Request $request): GenericFormToken
    {
        [$identifier, $password] = $this->authenticationRequest->extract($request);

        if (null === $identifier || null === $password) {
            throw new BadCredentials('Invalid credentials.');
        }

        return new GenericFormToken($identifier, $password, $this->providerKey);
    }
}
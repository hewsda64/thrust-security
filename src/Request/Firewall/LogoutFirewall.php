<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Authentication\TrustResolver;
use Thrust\Security\Contract\Http\Logout\Logout;
use Thrust\Security\Contract\Http\Request\AuthenticationRequest;
use Thrust\Security\Contract\Http\Response\LogoutSuccess;

class LogoutFirewall extends AuthenticationFirewall
{
    /**
     * @var array
     */
    private $handlers = [];

    /**
     * @var LogoutSuccess
     */
    private $successHandler;

    /**
     * @var TrustResolver
     */
    private $trustResolver;

    /**
     * @var AuthenticationRequest
     */
    private $authenticationRequest;

    /**
     * LogoutListener constructor.
     *
     * @param LogoutSuccess $successHandler
     * @param TrustResolver $trustResolver
     * @param AuthenticationRequest $authenticationRequest
     */
    public function __construct(LogoutSuccess $successHandler, TrustResolver $trustResolver, AuthenticationRequest $authenticationRequest)
    {
        $this->successHandler = $successHandler;
        $this->trustResolver = $trustResolver;
        $this->authenticationRequest = $authenticationRequest;
    }

    public function addHandler(Logout $logoutHandler): void
    {
        $this->handlers[] = $logoutHandler;
    }

    protected function processAuthentication(Request $request): ?Response
    {
        $response = $this->successHandler->onLogoutSuccess($request);

        $token = $this->token();

        foreach ($this->handlers as $handler) {
            $handler->logout($request, $response, $token);
        }

        $this->dispatchLogoutEvent($token);

        $this->eraseStorage();

        return $response;
    }

    protected function isRequired(Request $request): bool
    {
        if (!$this->hasToken() || $this->trustResolver->isAnonymous($this->token())) {
            return false;
        }

        return $this->authenticationRequest->matches($request);
    }
}
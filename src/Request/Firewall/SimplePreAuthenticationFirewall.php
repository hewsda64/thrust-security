<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Authentication\Authenticator\SimplePreAuthenticator;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Request\AuthenticationRequest;
use Thrust\Security\Contract\Http\Response\AuthenticationFailure;
use Thrust\Security\Contract\Http\Response\AuthenticationSuccess;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Foundation\Value\ProviderKey;

class SimplePreAuthenticationFirewall extends AuthenticationFirewall
{

    /**
     * @var ProviderKey
     */
    protected $providerKey;

    /**
     * @var SimplePreAuthenticator
     */
    protected $authenticator;

    /**
     * @var AuthenticationRequest
     */
    protected $authenticationRequest;

    /**
     * @var bool
     */
    protected $stateless;

    /**
     * SimplePreAuthenticationListener constructor.
     *
     * @param ProviderKey $providerKey
     * @param SimplePreAuthenticator $authenticator
     * @param AuthenticationRequest $authenticationRequest
     * @param bool $stateless
     */
    public function __construct(ProviderKey $providerKey, SimplePreAuthenticator $authenticator, AuthenticationRequest $authenticationRequest, bool $stateless)
    {
        $this->providerKey = $providerKey;
        $this->authenticator = $authenticator;
        $this->authenticationRequest = $authenticationRequest;
        $this->stateless = $stateless;
    }

    public function processAuthentication(Request $request): ?Response
    {
        try {
            $token = $this->authenticator->createToken($request, $this->providerKey);

            $token = $this->authenticate($token);

            return $this->onSuccess($token, $request);
        } catch (AuthenticationException $exception) {
            if ($this->authenticator instanceof AuthenticationFailure) {
                return $this->authenticator->onAuthenticationFailure($request, $exception);
            }

            return null;
        }
    }

    protected function onSuccess(Tokenable $token, Request $request): ?Response
    {
        $response = null;

        if ($this->authenticator instanceof AuthenticationSuccess) {
            $response = $this->authenticator->onAuthenticationSuccess($request, $token);
        }

        if (!$this->stateless) {
            $this->dispatchLoginEvent($request, $token);
        }

        if (!$response) {
            $this->setToken($token);
        }

        return $response;
    }

    public function isRequired(Request $request): bool
    {
        return !$this->hasToken() && $this->authenticationRequest->matches($request);
    }
}
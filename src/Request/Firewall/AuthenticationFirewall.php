<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Http\Firewall\Firewall;
use Thrust\Security\Foundation\Authorizer;
use Thrust\Security\Foundation\Guard;
use Thrust\Security\Foundation\SecurityEventable;

abstract class AuthenticationFirewall implements Firewall
{
    use Guard, Authorizer, SecurityEventable;

    public function handle(Request $request, \Closure $next)
    {
        if ($this->isRequired($request)) {
            $response = $this->processAuthentication($request);

            if ($response) {
                return $response;
            }
        }

        return $next($request);
    }

    abstract protected function isRequired(Request $request): bool;

    abstract protected function processAuthentication(Request $request): ?Response;
}
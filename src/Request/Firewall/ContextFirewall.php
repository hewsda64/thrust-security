<?php

declare(strict_types=1);

namespace Thrust\Security\Request\Firewall;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Http\Request;
use Thrust\Security\Contract\Http\Firewall\Firewall;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Contract\User\UserProvider;
use Thrust\Security\Event\ContextEventable;
use Thrust\Security\Exception\UnsupportedUser;
use Thrust\Security\Exception\UserNotFound;
use Thrust\Security\Foundation\Guard;
use Thrust\Security\Foundation\Value\ContextKey;

class ContextFirewall implements Firewall
{
    use Guard;

    /**
     * @var Dispatcher
     */
    private $events;

    /**
     * @var array
     */
    private $userProviders;

    /**
     * @var ContextEventable
     */
    private $event;

    /**
     * ContextListener constructor.
     *
     * @param ContextKey $contextKey
     * @param Dispatcher $events
     * @param array $userProviders
     */
    public function __construct(ContextKey $contextKey, Dispatcher $events, array $userProviders)
    {
        $this->events = $events;
        $this->userProviders = $userProviders;
        $this->event = new ContextEventable($contextKey);
    }

    public function handle(Request $request, \Closure $next)
    {
        $this->events->dispatch($this->event);

        if (null !== $token = $request->session()->get($this->event->sessionKey())) {

            $token = unserialize($token, [Tokenable::class]);

            if ($token instanceof Tokenable) {
                $this->setToken($this->refreshUser($token));
            } elseif (null !== $token) {
                $this->eraseStorage();
            }
        }

        return $next($request);
    }

    private function refreshUser(Tokenable $token): ?Tokenable
    {
        $user = $token->user();

        if (!$user instanceof User) {
            return $token;
        }

        $notFound = false;

        /** @var UserProvider $provider */
        foreach ($this->userProviders as $provider) {
            if (!$provider->supportsClass(get_class($user))) {
                continue;
            }

            try {
                $refreshedUser = $provider->refreshUser($user);

                $token->setUser($refreshedUser);

                return $token;

            } catch (UserNotFound $exception) {
                // to next provider
                $notFound = true;
            }
        }

        if($notFound){
            return null;
            // throw new UserNotFound('User not found.');
        }

        throw new UnsupportedUser(
            sprintf('No user provider supports the user class "%s"', get_class($user))
        );
    }
}
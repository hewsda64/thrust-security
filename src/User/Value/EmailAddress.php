<?php

declare(strict_types=1);

namespace Thrust\Security\User\Value;

use Thrust\Security\Contract\User\Value\EmailAddress as EmailAddressContract;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Contract\Value\SecurityValue;
use Thrust\Security\Foundation\Assertion\Assertion;

class EmailAddress implements EmailAddressContract, Identifier
{
    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    protected static $message = 'Email address provided is not valid.';

    /**
     * EmailAddress constructor.
     *
     * @param string $email
     */
    protected function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function fromString($email): self
    {
        Assertion::string($email, self::$message);
        Assertion::email($email, self::$message);

        return new self($email);
    }

    public function identify(): string
    {
        return $this->email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->email === $aValue->getEmail();
    }

    public function __toString(): string
    {
        return $this->email;
    }
}
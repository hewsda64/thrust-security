<?php

declare(strict_types=1);

namespace Thrust\Security\User\Value;

use Assert\Assertion;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Thrust\Security\Contract\User\Value\UserId as UserIdContract;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Contract\Value\SecurityValue;

class UserId implements UserIdContract, Identifier
{

    /**
     * @var UuidInterface
     */
    private $uid;

    /**
     * UserId constructor.
     *
     * @param UuidInterface $uid
     */
    private function __construct(UuidInterface $uid)
    {
        $this->uid = $uid;
    }

    public static function nextIdentity(): self
    {
        return new self(Uuid::uuid4());
    }

    public static function fromString($uid): self
    {
        Assertion::string($uid);
        Assertion::notEmpty($uid);

        return new self(Uuid::fromString($uid));
    }

    public function getUid(): UuidInterface
    {
        return $this->uid;
    }

    public function sameValueAs(SecurityValue $aValue): bool
    {
        return $aValue instanceof $this && $this->uid->equals($aValue->getUid());
    }

    public function identify(): string
    {
        return $this->uid->toString();
    }

    public function __toString(): string
    {
        return $this->identify();
    }
}
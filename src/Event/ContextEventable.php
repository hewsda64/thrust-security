<?php

declare(strict_types=1);

namespace Thrust\Security\Event;

use Thrust\Security\Foundation\Value\ContextKey;

class ContextEventable
{
    /**
     * @var ContextKey
     */
    protected $contextKey;

    /**
     * @var string
     */
    protected $sessionKey;

    /**
     * ContextEventable constructor.
     *
     * @param ContextKey $contextKey
     */
    public function __construct(ContextKey $contextKey)
    {
        $this->contextKey = $contextKey;
        $this->sessionKey = 'security_' . $contextKey->getKey();
    }

    public function contextKey(): ContextKey
    {
        return $this->contextKey;
    }

    public function sessionKey(): string
    {
        return $this->sessionKey;
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Event;

use Illuminate\Http\Request;
use Thrust\Security\Contract\User\User;

class UserWasImpersonated
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var User
     */
    protected $target;

    /**
     * UserWasImpersonated constructor.
     *
     * @param Request $request
     * @param User $targetUser
     */
    public function __construct(Request $request, User $targetUser)
    {
        $this->request = $request;
        $this->target = $targetUser;
    }

    public function request(): Request
    {
        return $this->request;
    }

    public function target(): User
    {
        return $this->target;
    }
}
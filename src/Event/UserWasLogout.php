<?php

declare(strict_types=1);

namespace Thrust\Security\Event;

use Thrust\Security\Contract\Token\Tokenable;

class UserWasLogout
{
    /**
     * @var Tokenable
     */
    protected $token;

    /**
     * UserWasLogout constructor.
     *
     * @param Tokenable $token
     */
    public function __construct(Tokenable $token)
    {
        $this->token = $token;
    }

    /**
     * @return Tokenable
     */
    public function token(): Tokenable
    {
        return $this->token;
    }
}
<?php

declare(strict_types=1);

namespace Thrust\Security\Event;

use Illuminate\Http\Request;
use Thrust\Security\Contract\Token\Tokenable;

class UserWasLogin
{
    /**
     * @var Request
     */
    public $request;

    /**
     * @var Tokenable
     */
    public $token;

    /**
     * UserWasLogin constructor.
     *
     * @param Request $request
     * @param Tokenable $token
     */
    public function __construct(Request $request, Tokenable $token)
    {
        $this->request = $request;
        $this->token = $token;
    }

    public function request(): Request
    {
        return $this->request;
    }

    public function token(): Tokenable
    {
        return $this->token;
    }
}
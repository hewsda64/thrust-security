<?php

declare(strict_types=1);

namespace ThrustTest\Security\Application;

use Illuminate\Container\Container;
use Illuminate\Contracts\Container\Container as ContainerContract;
use Illuminate\Foundation\Application;

trait CreateEmptyApplication
{
    public function createEmptyApplication(): Application
    {
        $app = new Application();
        $app->bind(ContainerContract::class, Container::class);

        return $app;
    }
}
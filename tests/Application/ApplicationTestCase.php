<?php

declare(strict_types=1);

namespace ThrustTest\Security\Application;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Events\Dispatcher;
use PHPUnit\Framework\TestCase;
use Thrust\Security\Contract\Authentication\Authenticatable;
use Thrust\Security\Contract\Authorization\Authorizable;
use Thrust\Security\Contract\Token\Storage\TokenStorage;
use Thrust\Security\Contract\Token\Tokenable;

class ApplicationTestCase extends TestCase
{
    use CreateEmptyApplication;

    /**
     * @var Application
     */
    protected $app;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Authenticatable
     */
    protected $guard;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Authorizable
     */
    protected $authorizer;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    protected $token;
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Dispatcher
     */
    protected $eventDispatcher;

    public function setUp(): void
    {
        $this->app = $this->createEmptyApplication();
    }

    public function setUpToken(): void
    {
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    public function setUpTokenStorage(): void
    {
        $this->tokenStorage = $this->getMockForAbstractClass(TokenStorage::class);

        $this->app->singleton(TokenStorage::class, function () {
            return $this->tokenStorage;
        });
    }

    public function setUpGuard(): void
    {
        $this->guard = $this->getMockForAbstractClass(Authenticatable::class);
        $this->app->bind(Authenticatable::class, function () {
            return $this->guard;
        });
    }

    public function setUpAuthorizer(): void
    {
        $this->authorizer = $this->getMockForAbstractClass(Authorizable::class);
        $this->app->bind(Authorizable::class, function () {
            return $this->authorizer;
        });
    }

    public function setUpEventDispatcher(): void
    {
        $this->eventDispatcher = $this->getMockBuilder(Dispatcher::class)
            ->disableOriginalConstructor()
            ->setMethods(['dispatch'])
            ->getMock();

        $this->app->singleton('events', function () {
            return $this->eventDispatcher;
        });
    }
}
<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Request\Firewall;

use Illuminate\Http\Request;
use Thrust\Security\Request\Firewall\AccessFirewall;
use ThrustTest\Security\Application\ApplicationTestCase;

class AccessFirewallTest extends ApplicationTestCase
{
    private $request;

    public function setUp(): void
    {
        parent::setUp();

        $this->setUpTokenStorage();
        $this->setUpToken();
        $this->setUpGuard();
        $this->setUpAuthorizer();
        $this->request = $this->getMockBuilder(Request::class)->getMock();
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\AuthenticationCredentialsNotFound
     */
    public function it_raise_exception_when_no_token_in_storage(): void
    {
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn(null);
        $this->token->expects($this->never())->method('isAuthenticated');

        $this->throughFirewall();
    }

    /**
     * @test
     */
    public function it_return_next_when_no_attributes_is_set(): void
    {
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn($this->token);
        $this->token->expects($this->never())->method('isAuthenticated');

        $this->throughFirewall();
    }

    /**
     * @test
     */
    public function it_process_authentication_when_token_is_not_authenticated(): void
    {
        $this->tokenStorage->expects($this->atLeastOnce())->method('getToken')->willReturn($this->token);
        $this->tokenStorage->expects($this->once())->method('setToken');
        $this->token->expects($this->atLeastOnce())->method('isAuthenticated')->willReturn(false);
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);
        $this->authorizer->expects($this->once())->method('decide')->willReturn(true);

        $this->throughFirewall(['foo']);
    }

    /**
     * @test
     */
    public function it_merge_attributes(): void
    {
        $middleware = new AccessFirewall(['foo']);

        $ref = new \ReflectionClass(AccessFirewall::class);
        $prop = $ref->getProperty('attributes');
        $prop->setAccessible(true);

        $this->assertCount(1, $prop->getValue($middleware));

        $middleware->setAttributes(['bar']);
        $this->assertCount(2, $prop->getValue($middleware));

        $middleware->setAttributes(['bar']);
        $this->assertCount(3, $prop->getValue($middleware));
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Contract\Exception\AuthorizationException
     */
    public function it_raise_exception_on_denied_authorization(): void
    {
        $this->token->expects($this->once())->method('isAuthenticated')->willReturn(true);
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn($this->token);
        $this->authorizer->expects($this->once())->method('decide')->willReturn(false);

        $this->throughFirewall(['foo']);
    }

    /**
     * @test
     */
    public function it_authorize_access(): void
    {
        $this->token->expects($this->once())->method('isAuthenticated')->willReturn(true);
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn($this->token);
        $this->authorizer->expects($this->once())->method('decide')->willReturn(true);

        $this->throughFirewall(['foo']);
    }

    public function throughFirewall(array $attributes = []): void
    {
        (new AccessFirewall($attributes))->handle($this->request, function(){});
    }
}
<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Request\Firewall;

use Illuminate\Http\Request;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Foundation\Value\AnonymousKey;
use Thrust\Security\Request\Firewall\AnonymousFirewall;
use ThrustTest\Security\Application\ApplicationTestCase;

class AnonymousFirewallTest extends ApplicationTestCase
{
    private $key;
    private $request;

    public function setUp(): void
    {
        parent::setUp();

        $this->setUpTokenStorage();
        $this->setUpToken();
        $this->setUpGuard();
        $this->key = $this->getMockBuilder(AnonymousKey::class)->disableOriginalConstructor()->getMock();
        $this->request = $this->getMockBuilder(Request::class)->getMock();
    }

    /**
     * @test
     */
    public function it_can_check_if_authentication_is_required(): void
    {
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn($this->token);
        $this->guard->expects($this->never())->method('authenticate');

        $this->throughFirewall();
    }

    /**
     * @test
     */
    public function it_authenticate(): void
    {
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn(null);
        $this->tokenStorage->expects($this->once())->method('setToken');
        $this->guard->expects($this->once())->method('authenticate')->willReturn($this->token);

        $this->throughFirewall();
    }

    /**
     * @test
     */
    public function it_catch_authentication_exception_when_authentication_fail(): void
    {
        $this->tokenStorage->expects($this->once())->method('getToken')->willReturn(null);
        $exc = new AuthenticationException('foo');
        $this->guard->expects($this->once())->method('authenticate')->willThrowException($exc);

        $this->throughFirewall();
    }

    public function throughFirewall(): void
    {
        (new AnonymousFirewall($this->key))->handle($this->request, function(){});
    }
}
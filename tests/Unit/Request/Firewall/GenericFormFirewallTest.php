<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Request\Firewall;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Thrust\Security\Contract\Exception\AuthenticationException;
use Thrust\Security\Contract\Http\Request\AuthenticationRequest;
use Thrust\Security\Contract\Http\Response\AuthenticationFailure;
use Thrust\Security\Contract\Http\Response\AuthenticationSuccess;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Request\Firewall\GenericFormFirewall;
use ThrustTest\Security\Application\ApplicationTestCase;

class GenericFormFirewallTest extends ApplicationTestCase
{
    /**
     * @test
     */
    public function it_can_check_if_authentication_is_required(): void
    {
        $this->authRequest->expects($this->once())->method('matches')->willReturn(false);
        $this->guard->expects($this->never())->method('authenticate');

        $this->throughFirewall();
    }

    /**
     * @test
     */
    public function it_authenticate_non_stateless_request(): void
    {
        $this->eventDispatcher->expects($this->once())->method('dispatch');
        $this->authRequest->expects($this->once())->method('matches')->willReturn(true);

        $id = $this->getMockForAbstractClass(Identifier::class);
        $cred = $this->getMockForAbstractClass(Credentials::class);

        $this->authRequest->expects($this->once())->method('extract')->willReturn([$id, $cred]);
        $this->tokenStorage->expects($this->once())->method('setToken');

        $redirect = $this->getMockBuilder(Response::class)->getMock();
        $this->success->expects($this->once())->method('onAuthenticationSuccess')->willReturn($redirect);

        $response = $this->throughFirewall();

        $this->assertSame($redirect, $response);
    }

    /**
     * @test
     */
    public function it_authenticate_stateless_request(): void
    {
        $this->eventDispatcher->expects($this->never())->method('dispatch');
        $this->authRequest->expects($this->once())->method('matches')->willReturn(true);

        $id = $this->getMockForAbstractClass(Identifier::class);
        $cred = $this->getMockForAbstractClass(Credentials::class);

        $this->authRequest->expects($this->once())->method('extract')->willReturn([$id, $cred]);
        $this->tokenStorage->expects($this->once())->method('setToken');

        $redirect = $this->getMockBuilder(Response::class)->getMock();
        $this->success->expects($this->once())->method('onAuthenticationSuccess')->willReturn($redirect);

        $response = $this->throughFirewall(TRUE);

        $this->assertSame($redirect, $response);
    }

    /**
     * @test
     */
    public function it_return_response_on_authentication_failure(): void
    {
        $this->eventDispatcher->expects($this->never())->method('dispatch');
        $this->authRequest->expects($this->once())->method('matches')->willReturn(true);

        $id = $this->getMockForAbstractClass(Identifier::class);
        $cred = $this->getMockForAbstractClass(Credentials::class);
        $this->authRequest->expects($this->once())->method('extract')->willReturn([$id, $cred]);

        $exc = new AuthenticationException('foobar');
        $this->guard->expects($this->once())->method('authenticate')->willThrowException($exc);

        $redirect = new Response('bar');
        $this->failure->expects($this->once())->method('onAuthenticationFailure')->willReturn($redirect);

        $response = $this->throughFirewall();

        $this->assertSame($redirect, $response);
    }

    public function throughFirewall(bool $stateless = false)
    {
        return (new GenericFormFirewall($this->key, $this->success, $this->failure, $this->authRequest, $stateless))
            ->handle($this->request, function () {
            });
    }

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|ProviderKey
     */
    private $key;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AuthenticationSuccess
     */
    private $success;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AuthenticationFailure
     */
    private $failure;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AuthenticationRequest
     */
    private $authRequest;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Request
     */
    private $request;

    public function setUp(): void
    {
        parent::setUp();

        $this->setUpTokenStorage();
        $this->setUpGuard();
        $this->setUpAuthorizer();
        $this->setUpEventDispatcher();

        $this->key = $this->getMockBuilder(ProviderKey::class)->disableOriginalConstructor()->getMock();
        $this->success = $this->getMockBuilder(AuthenticationSuccess::class)->disableOriginalConstructor()->getMock();
        $this->failure = $this->getMockBuilder(AuthenticationFailure::class)->disableOriginalConstructor()->getMock();
        $this->authRequest = $this->getMockForAbstractClass(AuthenticationRequest::class);
        $this->request = $this->getMockBuilder(Request::class)->getMock();
    }
}
<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication;

use ReflectionObject;
use Thrust\Security\Authentication\AuthenticationProviders;
use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Token\Tokenable;
use ThrustTest\Security\Unit\BaseTestCase;

class AuthenticationProvidersTest extends BaseTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AuthenticationProvider
     */
    private $provider;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    public function setUp(): void
    {
        $this->provider = $this->getMockForAbstractClass(AuthenticationProvider::class);
        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    /**
     * @test
     */
    public function it_can_be_constructed(): void
    {
        $providers = [$this->provider];
        $auth = new AuthenticationProviders($providers);
        $this->assertInstanceOf(AuthenticationProviders::class, $auth);
    }

    /**
     * @test
     */
    public function it_can_be_constructed_with_empty_array(): void
    {
        $providers = [];
        $auth = new AuthenticationProviders($providers);
        $this->assertInstanceOf(AuthenticationProviders::class, $auth);
    }

    /**
     * @test
     */
    public function it_add_authentication_provider(): void
    {
        $auth = new AuthenticationProviders([]);

        $refObject = new ReflectionObject($auth);
        $refProperty = $refObject->getProperty('authenticationProviders');
        $refProperty->setAccessible(true);

        $this->assertCount(0, $refProperty->getValue($auth));
        $auth->addProvider($this->provider);
        $this->assertCount(1, $refProperty->getValue($auth));
    }

    /**
     * @test
     */
    public function it_return_null_when_no_authentication_provider_support_token(): void
    {
        $providers = new AuthenticationProviders([]);

        $this->provider->expects($this->once())->method('supports')->willReturn(false);
        $providers->addProvider($this->provider);
        $this->assertNull($providers->getFirstProviderSupported($this->token));
    }

    /**
     * @test
     */
    public function it_return_first_supported_provider(): void
    {
        $providers = new AuthenticationProviders([]);

        $this->provider->expects($this->once())->method('supports')->willReturn(true);
        $providers->addProvider($this->provider);

        $aProvider = $this->getMockForAbstractClass(AuthenticationProvider::class);
        $aProvider->expects($this->never())->method('supports');
        $providers->addProvider($aProvider);

        $this->assertSame($this->provider, $providers->getFirstProviderSupported($this->token));
    }

    /**
     * @test
     */
    public function it_return_first_supported_provider_2(): void
    {
        $providers = new AuthenticationProviders([]);

        $aProvider = $this->getMockForAbstractClass(AuthenticationProvider::class);
        $aProvider->expects($this->once())->method('supports')->willReturn(false);
        $providers->addProvider($aProvider);

        $this->provider->expects($this->once())->method('supports')->willReturn(true);
        $providers->addProvider($this->provider);

        $this->assertSame($this->provider, $providers->getFirstProviderSupported($this->token));
    }
}
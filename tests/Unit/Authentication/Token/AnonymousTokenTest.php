<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token;

use Thrust\Security\Authentication\Token\AnonymousToken;
use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Foundation\Value\AnonymousKey;
use ThrustTest\Security\Unit\BaseTestCase;

class AnonymousTokenTest extends BaseTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AnonymousKey
     */
    private $key;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Identifier
     */
    private $identifier;

    protected function setUp(): void
    {
        $this->key = $this->getMockBuilder(AnonymousKey::class)->disableOriginalConstructor()->getMock();
        $this->identifier = $this->getMockForAbstractClass(Identifier::class);
    }

    /**
     * @test
     */
    public function it_can_be_constructed(): void
    {
        $token = new AnonymousToken($this->key, $this->identifier);

        $this->assertSame($this->key, $token->getKey());
    }

    /**
     * @test
     */
    public function it_return_empty_credentials(): void
    {
        $token = new AnonymousToken($this->key, $this->identifier);

        $this->assertInstanceOf(EmptyCredentials::class, $token->credentials());
    }
}
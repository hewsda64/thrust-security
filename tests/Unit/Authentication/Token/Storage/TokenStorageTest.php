<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token\Storage;

use Thrust\Security\Authentication\Token\Storage\TokenStorage;
use Thrust\Security\Contract\Token\Tokenable;
use ThrustTest\Security\Unit\BaseTestCase;

class TokenStorageTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_set_and_get_token_on_storage(): void
    {
        $storage = new TokenStorage();
        $token = $this->getMockForAbstractClass(Tokenable::class);

        $this->assertNull($storage->getToken());

        $storage->setToken($token);

        $this->assertSame($token, $storage->getToken());
    }

    /**
     * @test
     */
    public function it_erase_storage(): void
    {
        $storage = new TokenStorage();
        $token = $this->getMockForAbstractClass(Tokenable::class);
        $this->assertNull($storage->getToken());
        $storage->setToken($token);
        $this->assertNotNull($token, $storage->getToken());

        $storage->setToken();
        $this->assertNull($storage->getToken());
    }

}
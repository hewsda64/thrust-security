<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token\Value;

use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Authentication\Token\Value\SimpleCredentials;
use Thrust\Security\Contract\Value\Credentials;
use ThrustTest\Security\Unit\BaseTestCase;

class SimpleCredentialsTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_can_be_generated(): void
    {
        $cred = SimpleCredentials::fromString('foo');
        $this->assertInstanceOf(Credentials::class, $cred);
    }

    /**
     * @test
     */
    public function it_return_credentials(): void
    {
        $cred = SimpleCredentials::fromString('foo');
        $this->assertEquals('foo', $cred->getCredentials());
    }

    /**
     * @test
     * @dataProvider getWrongCredentialsValue
     *
     * @param $value
     */
    public function it_return_empty_credentials_instance_with_empty_or_null_value($value): void
    {
        $cred = SimpleCredentials::fromString($value);
        $this->assertInstanceOf(EmptyCredentials::class, $cred);
    }

    /**
     * @test
     */
    public function it_can_be_compared(): void
    {
        $cred = SimpleCredentials::fromString('foo');
        $cred2 = SimpleCredentials::fromString('foo');
        $cred3 = SimpleCredentials::fromString('bar');

        $this->assertTrue($cred->sameValueAs($cred2));
        $this->assertFalse($cred->sameValueAs($cred3));
    }

    /**
     * @test
     * @dataProvider getWrongCredentialsType
     * @expectedException \Thrust\Security\Foundation\Exception\SecurityValidationException
     *
     * @param $value
     */
    public function it_raise_exception_when_credentials_is_not_string($value): void
    {
        SimpleCredentials::fromString($value);
    }

    public function getWrongCredentialsValue(): array
    {
        return [[null], ['']];
    }

    public function getWrongCredentialsType(): array
    {
        return [[new \stdClass()], [array()]];
    }


}
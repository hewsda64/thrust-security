<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token\Value;

use Thrust\Security\Authentication\Token\Value\NullIdentifier;
use Thrust\Security\Contract\Value\Identifier;
use ThrustTest\Security\Unit\BaseTestCase;

class NullIdentifierTest extends BaseTestCase
{

    /**
     * @test
     */
    public function it_return_null_as_identity(): void
    {
        $id = new NullIdentifier();
        $this->assertNull($id->identify());
    }

    /**
     * @test
     */
    public function it_can_be_compared(): void
    {
        $id = new NullIdentifier();
        $id2 = new NullIdentifier();
        $aId = $this->getMockForAbstractClass(Identifier::class);

        $this->assertTrue($id->sameValueAs($id2));
        $this->assertFalse($id->sameValueAs($aId));

    }
}
<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token\Value;

use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Value\Credentials;
use ThrustTest\Security\Unit\BaseTestCase;

class EmptyCredentialsTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_return_an_empty_string(): void
    {
        $cred = new EmptyCredentials();
        $this->assertEmpty($cred->getCredentials());
    }

    /**
     * @test
     */
    public function it_can_be_compared(): void
    {
        $cred = new EmptyCredentials();
        $cred2 = new EmptyCredentials();
        $aCred = $this->getMockForAbstractClass(Credentials::class);
        $this->assertTrue($cred->sameValueAs($cred2));
        $this->assertFalse($cred->sameValueAs($aCred));
    }

    /**
     * @test
     */
    public function it_can_be_serialized(): void
    {
        $cred = new EmptyCredentials();
        $this->assertEquals('', (string)$cred);
    }
}
<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token\Value;

use Thrust\Security\Authentication\Token\Value\AnonymousIdentifier;
use ThrustTest\Security\Unit\BaseTestCase;

class AnonymousIdentifierTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_has_identity(): void
    {
       $id = new AnonymousIdentifier();
       $this->assertEquals($id::IDENTIFIER, $id->identify());
    }

    /**
     * @test
     */
    public function it_can_be_compared(): void
    {
        $id = new AnonymousIdentifier();
        $id2 = new AnonymousIdentifier();
        $this->assertTrue($id->sameValueAs($id2));
    }

    /**
     * @test
     */
    public function it_can_be_serialized(): void
    {
        $id = new AnonymousIdentifier();
        $this->assertEquals($id::IDENTIFIER, (string)$id);
    }
}
<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token;

use Thrust\Security\Authentication\Token\RecallerToken;
use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Foundation\Value\RecallerKey;
use ThrustTest\Security\Unit\BaseTestCase;

class RecallerTokenTest extends BaseTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|ProviderKey
     */
    private $providerKey;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|RecallerKey
     */
    private $recallerKey;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|User
     */
    private $user;

    protected function setUp(): void
    {
        $this->providerKey = $this->getMockBuilder(ProviderKey::class)->disableOriginalConstructor()->getMock();

        $this->recallerKey = $this->getMockBuilder(RecallerKey::class)->disableOriginalConstructor()->getMock();

        $this->user = $this->getMockForAbstractClass(User::class);
    }

    /**
     * @test
     */
    public function it_can_be_constructed_and_authenticated(): void
    {
        $token = new RecallerToken($this->user, $this->providerKey, $this->recallerKey);
        $this->assertTrue($token->isAuthenticated());
    }

    /**
     * @test
     */
    public function it_return_empty_credentials(): void
    {
        $token = new RecallerToken($this->user, $this->providerKey, $this->recallerKey);
        $this->assertInstanceOf(EmptyCredentials::class, $token->credentials());
    }

    /**
     * @test
     */
    public function it_return_provider_key(): void
    {
        $token = new RecallerToken($this->user, $this->providerKey, $this->recallerKey);
        $this->assertSame($this->providerKey, $token->providerKey());
    }

    /**
     * @test
     */
    public function it_return_recaller_key(): void
    {
        $token = new RecallerToken($this->user, $this->providerKey, $this->recallerKey);
        $this->assertSame($this->recallerKey, $token->recallerKey());
    }
}
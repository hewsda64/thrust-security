<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Token;

use Thrust\Security\Authentication\Token\GenericFormToken;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Contract\Value\Identifier;
use Thrust\Security\Foundation\Value\ProviderKey;
use ThrustTest\Security\Unit\BaseTestCase;

class GenericFormTokenTest extends BaseTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|ProviderKey
     */
    private $key;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Identifier
     */
    private $identifier;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Credentials
     */
    private $credentials;

    protected function setUp(): void
    {
        $this->key = $this->getMockBuilder(ProviderKey::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->identifier = $this->getMockForAbstractClass(Identifier::class);
        $this->credentials = $this->getMockForAbstractClass(Credentials::class);
    }

    /**
     * @test
     */
    public function it_can_be_constructed(): void
    {
        $token = new GenericFormToken($this->identifier, $this->credentials, $this->key);
        $this->assertFalse($token->isAuthenticated());
    }

    /**
     * @test
     */
    public function it_can_be_constructed_and_authenticated_with_roles(): void
    {
        $token = new GenericFormToken($this->identifier, $this->credentials, $this->key, ['ROLE_FOO']);
        $this->assertTrue($token->isAuthenticated());
    }

    /**
     * @test
     */
    public function it_return_provider_key(): void
    {
        $token = new GenericFormToken($this->identifier, $this->credentials, $this->key);
        $this->assertSame($this->key, $token->providerKey());
    }

    /**
     * @test
     */
    public function it_return_credentials(): void
    {
        $token = new GenericFormToken($this->identifier, $this->credentials, $this->key);
        $this->assertSame($this->credentials, $token->credentials());
    }

    /**
     * @test
     */
    public function it_erase_credentials(): void
    {
        $token = new GenericFormToken($this->identifier, $this->credentials, $this->key);

        $this->assertSame($this->credentials, $token->credentials());

        $token->eraseCredentials();

        $this->assertNotSame($this->credentials, $token->credentials());
    }
}
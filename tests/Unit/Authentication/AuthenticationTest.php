<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication;

use Thrust\Security\Authentication\Authentication;
use Thrust\Security\Authentication\AuthenticationProviders;
use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Token\Tokenable;
use ThrustTest\Security\Unit\BaseTestCase;

class AuthenticationTest extends BaseTestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|AuthenticationProviders
     */
    private $providers;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Tokenable
     */
    private $token;

    public function setUp(): void
    {
        $this->providers = $this->getMockBuilder(AuthenticationProviders::class)
            ->disableOriginalConstructor()
            ->setMethods(['getFirstProviderSupported'])
            ->getMock();

        $this->token = $this->getMockForAbstractClass(Tokenable::class);
    }

    /**
     * @test
     */
    public function it_authenticate_token(): void
    {
        $provider = $this->getMockBuilder(AuthenticationProvider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provider->expects($this->once())->method('authenticate')->willReturn($this->token);
        $this->providers->expects($this->once())->method('getFirstProviderSupported')->willReturn($provider);

        $m = new Authentication($this->providers);

        $this->assertSame($this->token, $m->authenticate($this->token));
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\UnsupportedAuthenticationProvider
     */
    public function it_raise_exception_when_no_provider_supports_token(): void
    {
        $this->providers->expects($this->once())->method('getFirstProviderSupported')->willReturn(null);
        $m = new Authentication($this->providers);
        $m->authenticate($this->token);
    }
}
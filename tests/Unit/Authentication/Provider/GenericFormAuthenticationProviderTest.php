<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication\Provider;

use Illuminate\Contracts\Hashing\Hasher;
use Thrust\Security\Authentication\Provider\GenericFormAuthenticationProvider;
use Thrust\Security\Authentication\Token\GenericFormToken;
use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Authentication\AuthenticationProvider;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Contract\User\LocalUser;
use Thrust\Security\Contract\User\UserChecker;
use Thrust\Security\Contract\User\UserProvider;
use Thrust\Security\Contract\User\Value\EncodedPassword;
use Thrust\Security\Exception\UserNotFound;
use Thrust\Security\Foundation\Value\ProviderKey;
use Thrust\Security\Role\Value\SwitchUserRole;
use ThrustTest\Security\Mock\SomeFakeToken;
use ThrustTest\Security\Unit\BaseTestCase;

class GenericFormAuthenticationProviderTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_can_authenticate_token(): void
    {
        $this->key->expects($this->any())->method('sameValueAs')->willReturn(true);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($this->localUser);
        $this->hasher->expects($this->once())->method('check')->willReturn(true);
        $auth = $this->getProvider();

        $this->assertInstanceOf(Tokenable::class, $auth->authenticate($this->getToken()));
    }

    /**
     * @test
     */
    public function it_can_add_impersonate_role(): void
    {
        $this->key->expects($this->once())->method('sameValueAs')->willReturn(true);
        $this->localUser->expects($this->any())->method('getRoles')->willReturn(['ROLE_BAR']);
        $this->userProvider->expects($this->never())->method('requireByIdentifier');
        $this->hasher->expects($this->never())->method('check');

        $credentials = $this->getMockForAbstractClass(EncodedPassword::class);
        $credentials->expects($this->atLeastOnce())->method('sameValueAs')->willReturn(true);

        $this->localUser->expects($this->atLeastOnce())->method('getPassword')->willReturn($credentials);

        $auth = $this->getProvider(FALSE);
        $role = new SwitchUserRole('ROLE_SWITCH_USER', $this->getToken());
        $token = new GenericFormToken($this->localUser, $credentials , $this->key, [$role]);

        $this->assertCount(1, $token->roles());

        $_token = $auth->authenticate($token);

        $found = false;
        foreach ($_token->roles() as $r) {
            if ($r instanceof SwitchUserRole) {
                $found = true;
            }
        }

        $this->assertTrue($found);
        $this->assertCount(2, $_token->roles());
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\BadCredentials
     */
    public function it_raise_exception_when_encoded_credentials_does_not_match(): void
    {
        $this->expectExceptionMessage('The credentials were changed from another session.');

        $this->key->expects($this->any())->method('sameValueAs')->willReturn(true);

        $this->userProvider->expects($this->never())->method('requireByIdentifier');
        $this->hasher->expects($this->never())->method('check');

        $anLocalUser = $this->getMockForAbstractClass(LocalUser::class);
        $credentials = $this->getMockForAbstractClass(EncodedPassword::class);

        $anLocalUser->expects($this->atLeastOnce())->method('getPassword')->willReturn($credentials);

        $token = new GenericFormToken($anLocalUser, $credentials, $this->key);

        $auth = $this->getProvider(FALSE);
        $auth->authenticate($token);
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\UnsupportedAuthenticationProvider
     */
    public function it_raise_exception_when_token_provider_key_is_different_from_authentication_provider_key(): void
    {
        $this->key->expects($this->any())->method('sameValueAs')->willReturn(false);
        $auth = $this->getProvider();
        $auth->authenticate($this->getToken());
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\UnsupportedAuthenticationProvider
     */
    public function it_raise_exception_when_token_is_not_supported(): void
    {
        $this->key->expects($this->any())->method('sameValueAs')->willReturn(false);
        $auth = $this->getProvider();
        $auth->authenticate(new SomeFakeToken());
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\UserNotFound
     */
    public function it_raise_exception_when_user_not_found(): void
    {
        $this->key->expects($this->any())->method('sameValueAs')->willReturn(true);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willThrowException(
            new UserNotFound('foo')
        );
        $this->hasher->expects($this->never())->method('check');
        $auth = $this->getProvider();
        $auth->authenticate($this->getToken());
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\BadCredentials
     */
    public function it_raise_exception_when_credentials_does_not_match(): void
    {
        $this->key->expects($this->any())->method('sameValueAs')->willReturn(true);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($this->localUser);
        $this->hasher->expects($this->once())->method('check')->willReturn(false);
        $auth = $this->getProvider(FALSE);
        $auth->authenticate($this->getToken());
    }

    /**
     * @test
     * @expectedException \Thrust\Security\Exception\BadCredentials
     */
    public function it_raise_exception_with_empty_credentials(): void
    {
        $this->expectExceptionMessage('The presented password cannot be empty.');

        $this->key->expects($this->any())->method('sameValueAs')->willReturn(true);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($this->localUser);
        $this->hasher->expects($this->never())->method('check');
        $auth = $this->getProvider(FALSE);

        $token = $this->getToken();
        $token->expects($this->once())->method('credentials')->willReturn(new EmptyCredentials());

        $auth->authenticate($token);
    }

    /**
     * @test
     */
    public function it_hide_bad_credentials_exception(): void
    {
        $this->expectExceptionMessage('User not found.');

        $this->key->expects($this->any())->method('sameValueAs')->willReturn(true);
        $this->userProvider->expects($this->once())->method('requireByIdentifier')->willReturn($this->localUser);
        $this->hasher->expects($this->never())->method('check');
        $auth = $this->getProvider(TRUE);

        $token = $this->getToken();
        $token->expects($this->once())->method('credentials')->willReturn(new EmptyCredentials());

        $auth->authenticate($token);
    }

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|ProviderKey
     */
    private $key;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|UserChecker
     */
    private $userChecker;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|UserProvider
     */
    private $userProvider;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|Hasher
     */
    private $hasher;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|LocalUser
     */
    private $localUser;

    protected function setUp(): void
    {
        $this->key = $this->getMockBuilder(ProviderKey::class)->disableOriginalConstructor()->getMock();
        $this->userChecker = $this->getMockForAbstractClass(UserChecker::class);
        $this->localUser = $this->getMockForAbstractClass(LocalUser::class);
        $this->userProvider = $this->getMockForAbstractClass(UserProvider::class);
        $this->hasher = $this->getMockForAbstractClass(Hasher::class);
    }

    public function getProvider(bool $hideException = true): AuthenticationProvider
    {
        return new GenericFormAuthenticationProvider($this->key, $this->userProvider, $this->userChecker, $this->hasher, $hideException);
    }

    /**
     * @return  \PHPUnit_Framework_MockObject_MockObject|GenericFormToken
     */
    public function getToken()
    {
        return $this->getMockBuilder(GenericFormToken::class)
            ->disableOriginalConstructor()
            ->getMock();
    }
}
<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Authentication;

use Thrust\Security\Authentication\DefaultTrustResolver;
use Thrust\Security\Authentication\Token\AnonymousToken;
use Thrust\Security\Authentication\Token\RecallerToken;
use Thrust\Security\Contract\Token\Tokenable;
use ThrustTest\Security\Unit\BaseTestCase;

class DefaultTrustResolverTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_check_if_token_is_recaller(): void
    {
        $resolver = $this->getTrustResolver();

        $this->assertFalse($resolver->isRememberMe());
        $this->assertFalse($resolver->isRememberMe($this->getMockToken()));
        $this->assertFalse($resolver->isRememberMe($this->getAnonymousToken()));
        $this->assertTrue($resolver->isRememberMe($this->getRecallerToken()));
    }

    /**
     * @test
     */
    public function it_check_if_token_is_anonymous(): void
    {
        $resolver = $this->getTrustResolver();

        $this->assertFalse($resolver->isAnonymous());
        $this->assertFalse($resolver->isAnonymous($this->getMockToken()));
        $this->assertFalse($resolver->isAnonymous($this->getRecallerToken()));
        $this->assertTrue($resolver->isAnonymous($this->getAnonymousToken()));
    }

    /**
     * @test
     */
    public function it_check_if_token_is_full_fledged(): void
    {
        $resolver = $this->getTrustResolver();

        $this->assertFalse($resolver->isFullFledged());
        $this->assertFalse($resolver->isFullFledged($this->getAnonymousToken()));
        $this->assertFalse($resolver->isFullFledged($this->getRecallerToken()));
        $this->assertTrue($resolver->isFullFledged($this->getMockToken()));
    }

    public function getMockToken(): \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockForAbstractClass(Tokenable::class);
    }

    public function getAnonymousToken(): \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockBuilder(AnonymousToken::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function getRecallerToken(): \PHPUnit_Framework_MockObject_MockObject
    {
        return $this->getMockBuilder(RecallerToken::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function getTrustResolver(): DefaultTrustResolver
    {
        return new DefaultTrustResolver(
            AnonymousToken::class,
            RecallerToken::class
        );
    }
}
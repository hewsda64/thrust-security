<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Event;

use Illuminate\Http\Request;
use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Event\UserWasLogin;
use ThrustTest\Security\Unit\BaseTestCase;

class UserWasLoginTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_return_request(): void
    {
        $req = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $token = $this->getMockForAbstractClass(Tokenable::class);

        $event = new UserWasLogin($req, $token);
        $this->assertSame($req, $event->request());
    }

    /**
     * @test
     */
    public function it_return_token(): void
    {
        $req = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $token = $this->getMockForAbstractClass(Tokenable::class);

        $event = new UserWasLogin($req, $token);
        $this->assertSame($token, $event->token());
    }
}
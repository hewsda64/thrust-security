<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Event;

use Thrust\Security\Contract\Token\Tokenable;
use Thrust\Security\Event\UserWasLogout;
use ThrustTest\Security\Unit\BaseTestCase;

class UserWasLogoutTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_return_token(): void
    {
        $token = $this->getMockForAbstractClass(Tokenable::class);
        $event = new UserWasLogout($token);
        $this->assertSame($token, $event->token());
    }
}
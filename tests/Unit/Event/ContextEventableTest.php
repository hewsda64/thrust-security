<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Event;

use Thrust\Security\Event\ContextEventable;
use Thrust\Security\Foundation\Value\ContextKey;
use ThrustTest\Security\Unit\BaseTestCase;

class ContextEventableTest extends BaseTestCase
{
    /**
     * @test
     */
    public function it_return_context_key(): void
    {
        $key = $this->getMockBuilder(ContextKey::class)->disableOriginalConstructor()->getMock();
        $event = new ContextEventable($key);
        $this->assertSame($key, $event->contextKey());
    }

    /**
     * @test
     */
    public function it_return_session_key(): void
    {
        $key = $this->getMockBuilder(ContextKey::class)->disableOriginalConstructor()->getMock();
        $key->expects($this->once())->method('getKey')->willReturn('foo');
        $event = new ContextEventable($key);

        $this->assertSame('security_foo', $event->sessionKey());
    }
}
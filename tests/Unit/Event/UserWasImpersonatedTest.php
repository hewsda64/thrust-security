<?php

declare(strict_types=1);

namespace ThrustTest\Security\Unit\Event;

use Illuminate\Http\Request;
use Thrust\Security\Contract\User\User;
use Thrust\Security\Event\UserWasImpersonated;
use ThrustTest\Security\Unit\BaseTestCase;

class UserWasImpersonatedTest extends BaseTestCase
{

    /**
     * @test
     */
    public function it_return_request(): void
    {
        $req = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $target = $this->getMockForAbstractClass(User::class);

        $event = new UserWasImpersonated($req, $target);

        $this->assertSame($req, $event->request());
    }

    /**
     * @test
     */
    public function it_return_target(): void
    {
        $req = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();
        $target = $this->getMockForAbstractClass(User::class);

        $event = new UserWasImpersonated($req, $target);

        $this->assertSame($target, $event->target());
    }
}
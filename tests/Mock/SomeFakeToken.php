<?php

declare(strict_types=1);

namespace ThrustTest\Security\Mock;

use Thrust\Security\Authentication\Token\Token;
use Thrust\Security\Authentication\Token\Value\EmptyCredentials;
use Thrust\Security\Contract\Value\Credentials;
use Thrust\Security\Foundation\Value\AnonymousKey;
use Thrust\Security\Foundation\Value\ProviderKey;

class SomeFakeToken extends Token
{

    public function credentials(): Credentials
    {
        return new EmptyCredentials();
    }

    public function getKey(): AnonymousKey
    {
        return new AnonymousKey('foo');
    }

    public function providerKey(): ProviderKey
    {
        return new ProviderKey('bar');
    }
}